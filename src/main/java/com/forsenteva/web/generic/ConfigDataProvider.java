package com.forsenteva.web.generic;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.forsenteva.web.library.GenericLib;

public class ConfigDataProvider 
{
	public static Properties pro;	   
	public ConfigDataProvider() 
	{		
		File src=new File(GenericLib.configPath);
		try 
		{
			FileInputStream fis=new FileInputStream(src);	
			pro=new Properties();
			pro.load(fis);
		} 
		catch (Exception e) 
		{
			System.out.println("Not able to load config file >> "+e.getMessage());
		}  
	}
	public static String getDataFromConfig(String keyToSearch) 
	{	
		return pro.getProperty(keyToSearch);	
	}
	public static String getBrowser() 
	{	
		return pro.getProperty("Browser1111");
	}	
	public static String getStagingURL() 
	{	
		return pro.getProperty("url111");
	}
}
