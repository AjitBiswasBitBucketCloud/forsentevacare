package com.forsenteva.web.generic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.forsenteva.web.library.GenericLib;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

public class ExcelDataProvider 
{
	static Workbook wb;
	static DataFormatter dataFormatter = new DataFormatter();
	public FileInputStream fis = null;
	static Sheet sheet;
	static Workbook wk;
	static Row row;
	static Cell cell;	
	/*
	 * 
	 * Description: Generic Method to create a File for the excel data
	 * 
	 */
	public ExcelDataProvider() 
	{
		File src=new File(GenericLib.testDataPath);
		try 
		{
			FileInputStream fis=new FileInputStream(src);
			wb = WorkbookFactory.create(fis);
		} 
		catch (Exception e) 
		{
			System.out.println("Unable to read excel file "+e.getMessage());
		} 
	}
	/*
	 *
	 * Description: To read the cell data in the form of a String 
	 * 
	 */
	public String getStringData(String sheetName, int row, int column) 
	{
		return wb.getSheet(sheetName).getRow(row).getCell(column).getStringCellValue();
	}
	/*
	 *
	 * Description: To read the cell data in the form of a Numeric value
	 * 
	 */
	public double getNumericData(String sheetName, int row, int column) {
	return wb.getSheet(sheetName).getRow(row).getCell(column).getNumericCellValue();
	}
	/*
	*
	* Description:To read test data from excel sheet based on TestcaseID
	* 
	*/
	public static String[] toReadExcelData(String sFilepath, String sSheet, String sTestCaseID)
	{
		String sData[] = null;
		try 
		{
			//File Read
			FileInputStream fis = new FileInputStream(sFilepath);
			//Workbook Create
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			//Specify Sheet
			Sheet sht = wb.getSheet(sSheet);
			//Row Count
			int iRowNum = sht.getLastRowNum();
			//Find out the testcase based on testcaseID
			for (int i = 0; i <= iRowNum; i++) 
			{
				//Match the Test Case ID
				if (sht.getRow(i).getCell(0).toString().equals(sTestCaseID)) 
				{
					//Cell Count of MAtched Row
					int iCellNum = sht.getRow(i).getPhysicalNumberOfCells();
					// Initialize the String Array Length
					sData = new String[iCellNum];
					//Loop TO Read Column Data And Store it in Array
					for (int j = 0; j < iCellNum; j++) 
					{
						Cell cell = sht.getRow(i).getCell(j);
						//Store Read Data Into Array
						sData[j] = dataFormatter.formatCellValue(cell);
					}
					break;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sData;
	}
	
public static int getRowCount(String path, String sheetname) throws IOException, Throwable, InvalidFormatException
{
	FileInputStream fis = new FileInputStream(path);
	//Workbook Create
	Workbook wb = (Workbook) WorkbookFactory.create(fis);
	//Specify Sheet
	Sheet sht = wb.getSheet(sheetname);
int rowcount=sheet.getLastRowNum();
wb.close();
fis.close();
return rowcount;
}
public static int getCellCount(String path, String sheetname,int rownum) throws IOException, Throwable, InvalidFormatException
{
	FileInputStream fis = new FileInputStream(path);
	//Workbook Create
	Workbook wb = (Workbook) WorkbookFactory.create(fis);
	//Specify Sheet
	Sheet sht = wb.getSheet(sheetname);
row=sheet.getRow(rownum); 
int cellcount=row.getLastCellNum();
wb.close();
fis.close();
return cellcount;
}
	/*
	*
	* Description:To read test data from excel sheet based on TestcaseID
	* 
	*/
	public static String[] toReadIntExcelData(String sFilepath, String sSheet, int sTestCaseID)
	{
		String sData[] = null;
		try 
		{
			//File Read
			FileInputStream fis = new FileInputStream(sFilepath);
			//Workbook Create
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			//Specify Sheet
			Sheet sht = wb.getSheet(sSheet);
			//Row Count
			int iRowNum = sht.getLastRowNum();
			//Find out the testcase based on testcaseID
			for (int i = 1; i <= iRowNum; i++) 
			{
				//Match the Test Case ID
				if (sht.getRow(i).getCell(0).toString().equals(sTestCaseID)) 
				{
					//Cell Count of MAtched Row
					int iCellNum = sht.getRow(i).getPhysicalNumberOfCells();
					// Initialize the String Array Length
					sData = new String[iCellNum];
					//Loop TO Read Column Data And Store it in Array
					for (int j = 0; j < iCellNum; j++) 
					{
						Cell cell = sht.getRow(i).getCell(j);
						//Store Read Data Into Array
						sData[j] = dataFormatter.formatCellValue(cell);
					}
					break;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sData;
	}
	
	
	
	
	/*
	*
	* 
	* Description: To get the Column Index based on the ColName
	* 
	*/
	public static int getColumnIndex(String filepath, String sSheet, String colName) 
	{
		//Read First Row as Excel Sheet Headings
		String[] firstRow = ExcelDataProvider.toReadExcelData(filepath, sSheet, "TEST_CASE_NO");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) 
		{
			if (firstRow[i].equalsIgnoreCase(colName)) 
			{
				index = i;
			}
		}
		return index;
	}
	/*
	 * 
	 * Description: To get ColumnData based on ColumnName
	 * 
	 */
	public static String[] getExcelData(String sPath, String sSheet, String colName) throws Throwable 
	{
		FileInputStream fis = new FileInputStream(sPath);
		Workbook wb = (Workbook) WorkbookFactory.create(fis);
		Sheet sht = wb.getSheet(sSheet);
		int rn = sht.getLastRowNum();
		String sdata[] = new String[rn+1];
		for (int i=0;i<rn ; i++)
		{
			int col = sht.getRow(i).getLastCellNum();

			for (int j=0 ;j< col ; j++)
			{
				if (sht.getRow(i).getCell(j).toString().equals(colName))
				{
					int n = j;
					int cn = sht.getPhysicalNumberOfRows();
					for(int k=1;k<cn;k++)
					{
						Cell cell1 = sht.getRow(k).getCell(n);
						sdata[k] = dataFormatter.formatCellValue(cell1);
					}
				}
			}
		}

		return sdata;    
	}
	/*
	 * 
	 * Description: To get the CellData based on the ColumnName with the specified Rownumber
	 * 
	 */
	public static  String getCellData(String spath,String sheetName, String coloumName, int rowNumber)
	{    
	       try
	       {
	       	FileInputStream fis = new FileInputStream(spath);
	       wk = WorkbookFactory.create(fis);
	           int colNum= -1;
	           sheet=wk.getSheet(sheetName);
	           row=sheet.getRow(0);

	           for(int i = 0; i<row.getLastCellNum(); i++)
	           {
	               if(row.getCell(i).getStringCellValue().trim().equals(coloumName))
	                   colNum=i;
	           }
	           row= sheet.getRow(rowNumber -1);
	           cell=row.getCell(colNum);

	           if(cell.getCellTypeEnum() == CellType.STRING)
	               return cell.getStringCellValue();
	           else if(cell.getCellTypeEnum()== CellType.NUMERIC || cell.getCellTypeEnum()== CellType.FORMULA )
	           {
	               String cellValue = String.valueOf(cell.getNumericCellValue());
	               if(HSSFDateUtil.isCellDateFormatted(cell))
	               {
	                   DateFormat dt = new SimpleDateFormat("dd/MM/yy");
	                   Date date = cell.getDateCellValue();
	                   cellValue = dt.format(date);
	               }
	               return cellValue;

	           }
	           else if(cell.getCellTypeEnum()== CellType.BLANK)
	               return "";
	           else
	               return String.valueOf(cell.getBooleanCellValue());        
	       }
	       catch(Exception e)
	       {
	           e.printStackTrace();
	           return "data not found";
	       }

	   } 
	/*
	 * 
	 * Desciption: To read cellData based on the ColumnName with the specified JiraId
	 * 
	 */
	   public static String readcolData (String TEST_CASE_NO ,String columnName,String spath,String sheetName) throws Exception 
	   {
	       String value = null;
	       FileInputStream fis = new FileInputStream(spath);
	       wk = WorkbookFactory.create(fis);
	       sheet = wk.getSheet(sheetName);
	      // XSSFRow row = sheet.getRow(0);
	      // XSSFCell cell= null;
	       for (int i=0;i<=sheet.getLastRowNum();i++) 
	       {    
	       //String d=getCellData(sheetName,"Page No." , i+1);
	       String d2=getCellData( spath,sheetName,"TEST_CASE_NO" , i+1);
	       //String[] d1=d.split(",");
	       if (d2.equals(TEST_CASE_NO)) 
	       {
	        
	        value= getCellData(spath,sheetName,columnName , i+1);
	        break;   
	       }
	       }
	       return value;       
	   }
	  /*
	   * 
	   *  Description: To write the integer Data based on the ColumnName
	   *  
	   */
	   public boolean setCellData(String sheetName, String colName, int rowNum, int value)
	    {
	        try
	        {
	        	
	            int col_Num = 0;
	            FileInputStream fis = new FileInputStream("C:\\Users\\Aatish\\Desktop\\aatish\\savitha\\evaCare\\src\\main\\resources\\testdata\\QuadWave.xlsx");
	            Workbook workbook = WorkbookFactory.create(fis);
				sheet = workbook.getSheet(sheetName);
				
	            row = sheet.getRow(0);
	            for (int i = 0; i < row.getLastCellNum(); i++) {
	                if (row.getCell(i).getStringCellValue().trim().equals(colName))
	                {
	                    col_Num = i;
	                }
	            } 
	            sheet.autoSizeColumn(col_Num);
	            row = sheet.getRow(rowNum - 1);
	            if(row==null)
	                row = sheet.createRow(rowNum - 1);
	 
	            cell = row.getCell(col_Num);
	            if(cell == null)
	                cell = row.createCell(col_Num);
	 
	            cell.setCellValue(value);
	 
	            FileOutputStream fos = new FileOutputStream("C:\\Users\\Aatish\\Desktop\\aatish\\savitha\\evaCare\\src\\main\\resources\\testdata\\QuadWave.xlsx");
	            workbook.write(fos);
	            fos.close();
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	            return  false;
	        }
	        return true;
	    }
	   /*
	    * 
	    * Description: To write the String Data based on the ColumnName
	    * 
	    */
	   public boolean setCellData(String sheetName, String colName, int rowNum, String value)
	   {
		   try
	        {
	        	
	            int col_Num = 0;
	            FileInputStream fis = new FileInputStream("C:\\Users\\Megha\\git\\repository\\ActiveTeach\\src\\test\\resources\\assetlocdata\\Assetloc.xlsx");
	            Workbook workbook = WorkbookFactory.create(fis);
				sheet = workbook.getSheet(sheetName);
				
	            row = sheet.getRow(0);
	            for (int i = 0; i < row.getLastCellNum(); i++) {
	                if (row.getCell(i).getStringCellValue().trim().equals(colName))
	                {
	                    col_Num = i;
	                }
	            } 
	            sheet.autoSizeColumn(col_Num);
	            row = sheet.getRow(rowNum - 1);
	          
	           if(row==null)
	                row = sheet.createRow(rowNum - 1);
	 
	            cell = row.getCell(col_Num);
	            if(cell == null)
	                cell = row.createCell(col_Num);
	 
	            cell.setCellValue(value);
	 
	            FileOutputStream fos = new FileOutputStream("C:\\Users\\Megha\\git\\repository\\ActiveTeach\\src\\test\\resources\\assetlocdata\\Assetloc.xlsx");
	            workbook.write(fos);
	            fos.close();
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	            return  false;
	        }
		return true;
		   
	   }
}
