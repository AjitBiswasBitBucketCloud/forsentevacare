package com.forsenteva.web.init;

import org.openqa.selenium.WebDriver;

import com.forsenteva.web.pages.Eva_Care_OnboardPatientPage1;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Login_Page;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class InitializePages
{
	public Login_Page oLogin_Page;
	public HomePage homepage;
	public OnBoardPatientPage onboardPatientpage;
	public static Eva_Care_OnboardPatientPage1 OnboardPatientPage;
	public InitializePages(WebDriver driver) 
	{	
		oLogin_Page = new Login_Page(driver);
		homepage = new HomePage(driver);
		onboardPatientpage= new OnBoardPatientPage(driver);
		OnboardPatientPage = new Eva_Care_OnboardPatientPage1(driver);
	}
}