package com.forsenteva.web.library;

public interface AutoConstant {
	String LOCAL_APPURL = "https://forsenteva.com";
	String LOCAL_OPERATINGSYSTEM = "WINDOWS";
	String LOCAL_BROWSERNAME = "chrome";
	String LOCALBROWSERVERSION = "76";
	String LOCAL_HUB_URL = "LOCAL";// http://localhost:4444/wd/hub";
//	String LOCAL_HUB_URL  = "http://D6eZSZTLxJ8MOyTVN3Gk5Gp4HRFIRlIl:iMa1CUC0tBD80mTEgqObW36tLC2tOAEW@SELENIUMGRID.gridlastic.com:80/wd/hub";
	String LOCAL_VIDEO_URL = "";
	String LOCAL_RECORDVIDEO = "No";
	String LOCAL_IMPLICITTIMEOUT = "10";
	String lOCAL_EXPLICITIMEOUTLOCAL = "10";
	String LOCAL_USERNAME = "ccaadmin123@wapl.ga";
	String LOCAL_PASSWORD = "password";
	String LOCAL_SUITENAME = "DefaultSuite";
	String LOCAL_USECASENAME = "";
	String LOCAL_TESTNAME = "DefaultTest";
	String XL_PATH = "./src/main/resources/testdata/eva.xlsx";
	String REPORT_PATH = "./Reports/";

}
