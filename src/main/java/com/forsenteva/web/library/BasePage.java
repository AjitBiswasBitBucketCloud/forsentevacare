package com.forsenteva.web.library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.forsenteva.web.library.Generic;
import com.forsenteva.web.library.WebActionUtil;

public class BasePage {
	
	public WebDriver driver;
	public WebActionUtil webActionUtil;
	public long ETO=0;
	public Generic generic;
	
	public BasePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		PageFactory.initElements(driver,this);
		this.webActionUtil=webActionUtil;
		this.ETO=ETO;
		this.generic=generic;
	}	
}
