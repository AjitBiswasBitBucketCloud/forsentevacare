package com.forsenteva.web.library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.seleniumhq.jetty9.server.HomeBaseWarning;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Login_Page;


import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest 
{
	public WebDriver driver;
	public String logPath = "";
	public boolean skipTest = false;
	public long ETO = 10, ITO = 10;
	public WebActionUtil webActionUtil;	
	public Generic generic = new Generic();	
	public ExcelDataProvider excelLibrary = new ExcelDataProvider();
	String sdata2;
	public static Properties prop;

	@BeforeTest
	public void _LaunchApp() throws Exception {
		
		try {
			prop=new Properties();
			FileInputStream fis=new FileInputStream(GenericLib.configPath);
			prop.load(fis);
			
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
			String browser=prop.getProperty("Browser");
		
			if (browser.equalsIgnoreCase("firefox"))
			{

				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.addPreference("dom.webnotifications.enabled", false);
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver(firefoxOptions);			
			} 
			else if (browser.equalsIgnoreCase("safari")) 
			{
				driver = new SafariDriver();
				generic.logMessage(LogStatus.INFO, "Running in the 'Safari' Browser - Local");
			} 
			else
			{
				generic.logMessage(LogStatus.INFO, "Run in Chrome");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");
				WebDriverManager.chromedriver().arch64().arch32().setup();
				driver = new ChromeDriver(options);
				generic.logMessage(LogStatus.INFO, "Running in the 'Chrome' Browser - Local");
			}
			driver.manage().timeouts().implicitlyWait(ITO, TimeUnit.SECONDS);
			webActionUtil = new WebActionUtil(driver, ETO, generic);
		}

	
	@AfterClass()
	public void _CloseApp() {

		/* Close the browser */
	try {
			if (driver != null) {
				driver.close();
				generic.logMessage(LogStatus.INFO, "Closing Browser");
			} else {
				generic.logMessage(LogStatus.ERROR, "@AfterClass driver instance is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
			generic.logMessage(LogStatus.ERROR, e.getMessage());
		}



	}

	
	
	@BeforeMethod
	public void _SignInToApp() throws Throwable {
		
		HomePage home=new HomePage(driver);

		/* Enter the application URL and login to the application */
		try {
			if (skipTest) {
				generic.logMessage(LogStatus.WARNING, "Skipping BeforeMethod");
			} else {
				generic.logMessage(LogStatus.INFO, "Open the eva Application...");
				
				driver.get(prop.getProperty("url"));
				driver.manage().window().maximize();

				Login_Page loginPage = new Login_Page(driver);
				generic.logMessage(LogStatus.INFO, "Enter the Credentials");
				InitializePages pages=new InitializePages(driver);
				
				int indexOfUserName=ExcelDataProvider.getColumnIndex(GenericLib.testDataPath, "Sheet1", "User");
				int indexOfpass=ExcelDataProvider.getColumnIndex(GenericLib.testDataPath, "Sheet1", "pass");
				
				 String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath, "Sheet1", "TC_001");
				 String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath, "Sheet1", "TC_001");
				 loginPage.loginToApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
				  	
				// String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");
								
				 //for(int i=1; i<sdata1.length; i++)
				 //{
					 // sdata2 = ExcelDataProvider.readcolData(sdata1[i], "FName", GenericLib.testDataPath1, "Sheet1");	 
					//	System.out.println(sdata2);
									
					//	 String Last_Name = ExcelDataProvider.readcolData(sdata1[i], "Lname", GenericLib.testDataPath1, "Sheet1");	 
						//	System.out.println(Last_Name);
						//	 String Email_Id = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.testDataPath1, "Sheet1");	 
						//		System.out.println(Email_Id);
							//	 String Phone_No = ExcelDataProvider.readcolData(sdata1[i], "phone", GenericLib.testDataPath1, "Sheet1");	 
							//		System.out.println(Phone_No);
								//	 String Emergency_Name = ExcelDataProvider.readcolData(sdata1[i], "Ephone", GenericLib.testDataPath1, "Sheet1");	 
								//		System.out.println(Emergency_Name);
								//		 String Emergency_No = ExcelDataProvider.readcolData(sdata1[i], "Econtact", GenericLib.testDataPath1, "Sheet1");	 
								//			System.out.println(Emergency_No);
								//			 String sdata8 = ExcelDataProvider.readcolData(sdata1[i], "DOB", GenericLib.testDataPath1, "Sheet1");	 
									//			System.out.println(sdata8);
									//			HomePage homepage=new HomePage(driver);	
									//	homepage.addNewPatient(sdata2, Last_Name, Email_Id, Phone_No, Emergency_Name, Emergency_No);
										
				 }
				
				 //String[] Sdata10=sdata2.split("\n");
					
					//System.out.println("hello"+Sdata10.length);
			}
			
		catch (Exception e) {
			e.printStackTrace();
			generic.logMessage(LogStatus.ERROR, e.getMessage());
		}
	


	}

	
}
