package com.forsenteva.web.library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excell {
public static FileInputStream fis;
public static FileOutputStream fos;
public static XSSFWorkbook workbook;
public static XSSFSheet sheet;
public static XSSFRow row;
public static XSSFCell cell;

public static int getRowCount(String path, String sheetname) throws IOException
{
	fis=new FileInputStream(path);
	workbook=new XSSFWorkbook(fis);
	sheet=workbook.getSheet(sheetname);
	int rowcount=sheet.getLastRowNum();
	workbook.close();
	fis.close();
	return rowcount;
}
public static int getCellCount(String path, String sheetname,int rownum) throws IOException
{
	fis=new FileInputStream(path);
	workbook=new XSSFWorkbook(fis);
	sheet=workbook.getSheet(sheetname);
	row=sheet.getRow(rownum);
	int cellcount=row.getLastCellNum();
	workbook.close();
	fis.close();
	return cellcount;
}
public static String getCellData(String path, String sheetname,int rownum,int colmn) throws IOException
{
	fis=new FileInputStream(path);
	workbook=new XSSFWorkbook(fis);
	sheet=workbook.getSheet(sheetname);
	row=sheet.getRow(rownum);
	cell=row.getCell(colmn);
	String data;
	try {
		DataFormatter format=new DataFormatter();
		String cellData=format.formatCellValue(cell);
		return cellData;
	}catch(Exception e)
	{
		data="";
	}
	workbook.close();
	fis.close();
	return data;
}
public static void setCellData(String path, String sheetname,int rownum,int colmn,String setingCelldata) throws IOException
{
	fis=new FileInputStream(path);
	workbook=new XSSFWorkbook(fis);
	sheet=workbook.getSheet(sheetname);
	row=sheet.getRow(rownum);
	cell=row.createCell(colmn);
	cell.setCellValue(setingCelldata);
	fos=new FileOutputStream(path);
	workbook.write(fos);
	workbook.close();
	fis.close();
	fos.close();
	
}
}
