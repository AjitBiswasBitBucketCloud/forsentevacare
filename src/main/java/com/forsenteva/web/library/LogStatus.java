package com.forsenteva.web.library;

public enum LogStatus { 
	ERROR,
	FAIL, 
	FATAL, 
	INFO, 
	PASS, 
	SKIP,
	WARNING
}
