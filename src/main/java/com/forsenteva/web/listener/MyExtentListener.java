package com.forsenteva.web.listener;



import java.io.File;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.forsenteva.web.generic.ConfigDataProvider;
import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.Helper;
import com.forsenteva.web.library.WebActionUtil;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class MyExtentListener implements ITestListener 
{
	public ExcelDataProvider excel;
	public ConfigDataProvider config;
	public static ExtentReports report;
	public static ExtentTest logger;
	public ExtentHtmlReporter extent;
	String className;
	public static int iPassCount=0;
	public static int iFailCount=0;
	public static int iSkippedCount=0;
	public static String profile=null;
	public static ArrayList sTestName= new ArrayList<String>();
	public static ArrayList sStatus= new ArrayList<String>();
	public static long totPassedTime=0;
	public static long totFailedTime=0;
	public static long totSkippedTime=0;
	public static long totalTimeTaken=0;
	public static String pass_Time = "0";
	public static String fail_Time = "0";
	public static String skip_Time = "0";
	
	
	static {
		System.out.println();
		profile = System.getProperty("profile");
			profile = "SanityEndToEnd";
			System.setProperty("profile", profile);
			System.out.println("Running locally " + profile);
		}
	static Map<String, String> reportMailBody = new HashMap<String, String>();
	File pdfReports = new File("C:\\Users\\Savitha\\Desktop\\evaCare1\\PDFReports" + "\\sample" + ".pdf");
	public void onTestStart(ITestResult result) 
	{
		// TODO Auto-generated method stub
		className = result.getTestClass().getName().toString(); 
		className = className.substring(className.lastIndexOf(".") + 1, className.length());
		logger=report.createTest(className);		
	}
	public void onTestSuccess(ITestResult result)
	{
		iPassCount = iPassCount+1;
		setStatus(result.getName().toString(), "Passed",sTestName,sStatus);
		for(int i=0;i<sTestName.size();i++){
			String s1=(String) sTestName.get(i);
			String s2=(String) sStatus.get(i);
			reportMailBody.put(s1, s2);
		}
		
		long scriptTime=result.getEndMillis() - result.getStartMillis();
		totPassedTime = totPassedTime + scriptTime;
		pass_Time = WebActionUtil.formatDuration(totPassedTime);
		
		
		// TODO Auto-generated method stub
		//logger.pass(MarkupHelper.createLabel(className + " case has PASSED", ExtentColor.GREEN));		
	}
	public void onTestFailure(ITestResult result)
	{
		logger.fail(MarkupHelper.createLabel(className + " test script has FAILED", ExtentColor.RED)); 
		// TODO Auto-generated method stub		
		iFailCount = iFailCount+1;
		setStatus(result.getTestClass().getRealClass().getSimpleName().toString(), result.getThrowable().getLocalizedMessage(),sTestName,sStatus);
		for(int i=0;i<sTestName.size();i++){
			String s1=(String) sTestName.get(i);
			String s2=(String) sStatus.get(i);
			reportMailBody.put(s1, s2);
		}
		long scriptTime=result.getEndMillis() - result.getStartMillis();
		totFailedTime = totFailedTime + scriptTime;
		fail_Time = WebActionUtil.formatDuration(totFailedTime);
		System.out.println("GetLocalized================= "+ result.getThrowable().getLocalizedMessage());
		System.out.println("To String=================" + result.getThrowable().toString());
		System.out.println("Get Message=================" + result.getThrowable().getMessage());
		
	}
	public void onTestSkipped(ITestResult result)
	{		
		iSkippedCount = iSkippedCount+1;
		setStatus(result.getName(), "Skipped",sTestName,sStatus);
		for(int i=0;i<sTestName.size();i++){
			String s1=(String) sTestName.get(i);
			String s2=(String) sStatus.get(i);
			reportMailBody.put(s1, s2);
		}
		long scriptTime=result.getEndMillis() - result.getStartMillis();
		totSkippedTime = totSkippedTime + scriptTime;
		skip_Time = WebActionUtil.formatDuration(totSkippedTime);
		// TODO Auto-generated method stub		
	}
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0)
	{		
		// TODO Auto-generated method stub		
	}
	public void onStart(ITestContext context)
	{
	extent= new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "/Reports/DemoQuadwave" + Helper.getCurrentDateTime() + ".html"));
	extent.config().setReportName("ForsentEva Report");
	extent.config().setDocumentTitle("ForsentEva Automation Report");
	extent.config().setTheme(Theme.DARK);
	report = new ExtentReports();
	report.attachReporter(extent);
	/*logger = report.createTest("Login");
	logger.info(MarkupHelper.createLabel("Setting up reports and test is getting ready", ExtentColor.AMBER));
	Reporter.log("Setting up reports and test is getting ready", true);
	

	Reporter.log("Setting Done- Test can be started", true);
	logger.info(MarkupHelper.createLabel("Setting Done- Test can be started", ExtentColor.AMBER));
		// TODO Auto-generated method stub
*/		
	}
	public void onFinish(ITestContext context)
	{
		// TODO Auto-generated method stub
		
		 iPassCount = context.getPassedTests().size(); 
		 iFailCount = context.getFailedTests().size(); 
		 iSkippedCount = context.getSkippedTests().size(); 
		int iTotal_Executed = iPassCount+iFailCount+iSkippedCount;
		totalTimeTaken=totPassedTime + totFailedTime + totSkippedTime;
		sendMail(iPassCount, iFailCount, iSkippedCount, iTotal_Executed, pdfReports,profile);
		report.flush();		
	}
	public static void sendMail(int iPassCount, int iFailCount, int skippedCount, int iTotal_Executed, File pdfReports,
			String profile) {

		String host = "smtp.gmail.com";
		int portNo = 465;
		String emailFrom = "savitha.k01@testyantra.com";
		String emailTo = "savitha.k01@testyantra.com";
		// String emailBcc = "aatish.s@testyantra.com";
		String emailCc = "savitha.k01@testyantra.com";
		String Subject = "TEST EMAIl PLEASE IGNORE";
		String UN = "savitha.k01@testyantra.com";
		String PWD = "Savikjan1";
		String msgBody = "This is a test mail please Ignore";

		Properties props = new Properties();
		System.out.println("Executing sendMail method");
		Date date = new Date();
		SimpleDateFormat sdtf = new SimpleDateFormat("dd-MM-yyyy_hh_mm_ss");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String sdateTime = sdtf.format(date);
		String sdate = sdf.format(date);
		String message = "<p>Hi Team,</p><div style=\"font-family:Verdana;\"> " + profile
				+ " execution on " + "PROD"
				+ " Environment , PFA for high level information</div><p></p><p></p><p></p><p></p>"
				+ "<p><div style=\"font-family:Verdana;\"><b> EXECUTION SUMMARY : </b></div></p>"
				+ "<table bgcolor=\"#BDE4F6\" style=\"border-radius: 20px; padding: 25px;\"><tr><td>&nbsp;&nbsp;&nbsp;<table style=\"height:180px; width:200px; border-width:2px; border-style:groove; float: left\"><tbody>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Total Executed</th><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ iTotal_Executed + "&nbsp;&nbsp;</td><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;" + 
						totalTimeTaken + "&nbsp;&nbsp;</td></tr>"
				+ "<tr style=\"outline: thin solid;  font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Passed</th><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ iPassCount + "&nbsp;&nbsp;</td><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;" + 
				pass_Time + "&nbsp;&nbsp;</td></tr>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Failed</th><td style=\"color: Red; outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ iFailCount + "&nbsp;&nbsp;</td><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;" + 
						fail_Time + "&nbsp;&nbsp;</td></tr>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Skipped</th><td style=\"color: Red; outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ iSkippedCount + "&nbsp;&nbsp;</td><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;" + 
				skip_Time + "&nbsp;&nbsp;</td></tr>";
		
		message = message
				+ "<table bgcolor=\"#BDE4F6\" style=\"border-radius: 20px; padding: 25px;\"><tr><td><table style=\"width:800px; border-width:2px; border-style:groove; float: left\"><tbody>"
				+ "<tr style=\"outline: thin solid;  font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Time Taken For Passed Script</th><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ totPassedTime + "&nbsp;&nbsp;</td></tr>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Time Taken For Failed Script</th><td style=\"color: Red; outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ totFailedTime + "&nbsp;&nbsp;</td></tr>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Time Taken For Skipped Script</th><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ totSkippedTime + "&nbsp;&nbsp;</td></tr>" + "</tbody></table></td>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; \"><th style=\"outline: thin solid;\">Total Time Taken For Suite Execution</th><td style=\"outline: thin solid; font-weight: bold;\">&nbsp;&nbsp;"
				+ totalTimeTaken + "&nbsp;&nbsp;</td></tr>" + "</tbody></table></td>" + "&nbsp;&nbsp;&nbsp;";

		
		message = message
				+ "<table bgcolor=\"#BDE4F6\" style=\"border-radius: 20px; padding: 25px;\"><tr><td><table style=\"width:800px; border-width:2px; border-style:groove; float: left\"><tbody>"
				+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; width:400px; \"><th style=\"outline: thin solid;\">Test Case</th>"
				+ "<th style=\"outline: thin solid; width:400px; \">Reason for failure</th></tr>";
		for (@SuppressWarnings("rawtypes")
		Map.Entry entry : reportMailBody.entrySet()) {
			System.out.println(entry.getKey() + " :" + entry.getValue());
			message = message
					+ "<tr style=\"outline: thin solid; font-family:Verdana; color: #000000; text-align: left; border-width:2px; width:400px; \">"
					+ "<td style=\"outline: thin solid; width:2px; border-style:groove; float: left  width:400px; \">"
					+ entry.getKey()
					+ "</td> <td style=\"outline: thin solid; width:2px; border-style:groove; float: left  width:600px; \">"
					+ entry.getValue() + "</td>" + " </tr>";
		}
		message = message + "</tbody></table></td></tr></table>"
				+ "<p></p><div style=\"font-family:Verdana;\">Regards,</div><p></p>"
				+ "<div style=\"font-family:Verdana;\">Test Yantra Software Automation Team</div>";

		/*
		 * props.put("mail.smtp.auth", "true"); props.put("mail.smtp.starttls.enable",
		 * "true"); props.put("mail.smtp.host", "smtp.gmail.com");
		 * //props.put("mail.smtp.port", "465"); props.put("mail.smtp.port", "587");
		 * props.put("mail.debug", "true");
		 */

		props.put("mail.smtp.user", "Savitha Kiran");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "25");
		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.EnableSSL.enable", "true");

		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("savitha.k01@testyantra.com", "Savikjan1");
			}
		});

		/*
		 * Session session = Session.getInstance(props,new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("savitha.k01@testyantra.com", "Savikjan1"); } });
		 */
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("savitha.k01@testyantra.com"));
			msg.setRecipients(Message.RecipientType.TO, "kishor.k@testyantra.com");
			msg.addRecipients(Message.RecipientType.TO, "shiva.g@testyantra.com , aatish.s@testyantra.com");
			msg.setSubject(
					"EvaCare Run For " + "Testcases" + "" + " on " + "PROD" + " Env " + sdate);
			msg.setSentDate(new Date());
			Multipart multipart = new MimeMultipart();
			MimeBodyPart textPart = new MimeBodyPart();
			textPart.setContent(message, "text/html");
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource("C:\\Users\\Savitha\\eclipse-workspace\\email1\\src\\test\\java\\images\\PieChart.png");
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");
			multipart.addBodyPart(textPart);
			multipart.addBodyPart(messageBodyPart);
			/*MimeBodyPart attachementPart = new MimeBodyPart();
			attachementPart.attachFile(pdfReports);
			multipart.addBodyPart(attachementPart);*/
			msg.setContent(multipart);
			Transport.send(msg);
			System.out.println("Mail Sent Successfully");

			/*
			 * Email email = new SimpleEmail(); System.out.println("email iniated");
			 * email.setHostName(host); email.setSmtpPort(portNo);
			 * email.setSSLOnConnect(true); email.setAuthentication(UN, PWD);
			 * email.setFrom(emailFrom); email.addTo(emailTo); //email.addBcc(emailBcc);
			 * //email.addCc(emailCc); email.setSubject("Test Mail!");
			 * email.setMsg(message);
			 * 
			 * System.out.println("Sending!!!"); email.send();
			 * 
			 * System.out.println("email sent successfully");
			 */

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void setStatus(String sName, String sResult, ArrayList sTestName, ArrayList sStatus) {
		sName = sName.replace("test", "");
		sTestName.add(sName);
		sStatus.add(sResult);

		if (sResult.equals("Passed")) {
			iPassCount = iPassCount + 1;
		} else if (sResult.equals("Failed")) {
			iFailCount = iFailCount + 1;
		} else {
			iSkippedCount = iSkippedCount + 1;
		}
	}
}
