package com.forsenteva.web.pages;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.library.WebActionUtil;

public class Eva_Care_OnboardPatientPage1 extends BasePage{
	String str;
	public Eva_Care_OnboardPatientPage1(WebDriver driver) {
		super(driver);
		
	}
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	
	@FindBy(xpath = "//a[contains(text(),'Onboard Patient')]")//getting text from on board patient 
	private WebElement gettext;
	
	/*
	 * 2 testcase webelements
	 */ 
	@FindBy(xpath = "//span[contains(text(),'Add New')]")
	private WebElement addNew;
	@FindBy(xpath = "//input[@name='Filter']")
	private WebElement serachfield;
	@FindBy(xpath = "//button[@id='Search']")
	private WebElement searchbtn;
	@FindBy(xpath = "//div[@id='form-group-PatientsList']/descendant::table/descendant::th[@class='ng-binding ng-scope']")
	private List<WebElement> list;
	
	/*
	 * 3 testcase  webelements
	 */
	@FindBy(xpath = "//input[@name='FirstName']")
	private WebElement fName;
	
	@FindBy(xpath = "//input[@name='LastName']")
	private WebElement lName;
	
	/*
	 * 4 testcase  webelements
	 */
	@FindBy(xpath="//label[contains(text(),'First Name : ')]")
	WebElement firstname;
	@FindBy(xpath="//label[contains(text(),'Last Name : ')]")
	WebElement lastname;
	@FindBy(xpath="//label[contains(text(),'Email : ')]")
	WebElement email;
	@FindBy(xpath="//label[contains(text(),'Gender :')]")
	WebElement gender;
	@FindBy(xpath="//label[contains(text(),'Date of Birth : ')]")
	WebElement dob;
	
	@FindBy(xpath="//label[contains(text(),'Phone Number : ')]")
	WebElement phonenum;
	@FindBy(xpath="//label[contains(text(),'Emergency Phone No. : ')]")
	WebElement ephonenum;
	@FindBy(xpath="//label[contains(text(),'Emergency Contact : ')]")
	WebElement econtact;
	@FindBy(xpath="//label[contains(text(),'Status : ')]")
	WebElement status;
	
	@FindBy(xpath="//span[@id='selectcaregiver']")
	WebElement selectcaregiver;
	
	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
	WebElement selectbtn;
	@FindBy(xpath="//span[contains(text(),'Cancel')]")
	WebElement cancelbtn;
	
	/*
	 * 5 testcase webelements
	 */
	@FindBy(xpath = "//input[@id='Email']")
	private WebElement eMail;
	@FindBy(xpath="//div[@id='Gender']/div/span/i")
	private WebElement gender1;
	@FindBy(xpath = "//button[@class='btn btn-default']")
	private WebElement clalender;
	
	@FindBy(xpath="//strong[@class='ng-binding']")
	private WebElement monthclick;
	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement ePhoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactName']")
	private WebElement eContactName;
	@FindBy(xpath="//span[contains(text(),'next')]")
	private WebElement next;
	
	@FindBy(xpath="//input[@name='CareGiver-115']")
	private WebElement caregiver;
	
	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
	private WebElement submit;
	
	
	/*
	 * 6 testcase webelements
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-FirstName']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement fnameasstric;
	
	
	/*
	 * 9th testcase webelement
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-LastName']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement lastnamestrick;

	/*
	 * 12th testcase webelement
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-Email']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement emailastrick;
	
	
	/*
	 * 13 th test case web elements
	 */
	@FindBy(xpath = "//p[contains(text(),'Please Enter Email')]")
	private WebElement eMailtext;
	
	/*
	 * 14 th test case webelements
	 */
	@FindBy(xpath = "//p[contains(text(),'Please Enter Email')]")
	private WebElement emailvaldationmsg;
	
	/*
	 * 15 th test case webelements
	 */
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-1']/span/formio-select-item/span")
	private WebElement gendermale;
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-0']/span/formio-select-item/span")
	private WebElement genderfemale;
	
	/*
	 * 17 th test case web elements
	 */
	@FindBy(xpath="//span[contains(text(),'Female')]")
	private WebElement femalevalue;
	
	/*
	 * 18 th testcase web elements
	 */
	@FindBy(xpath="//span[contains(text(),'Male')]")
	private WebElement malevalue;
	
	/*
	 * 19 th test case WebElements
	 */
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar ng-scope']")
	private WebElement dateicon;
	@FindBy(xpath = "//button[@class='btn btn-default btn-sm uib-title']")
	private WebElement opencalender;
	
	
	/*
	 * 71th testcase web elements
	 */
	@FindBy(xpath = "//button[@id='PatientsList.PersonalInterests-0-6']")
	private WebElement pinterest;
	
	@FindBy(xpath = "//input[@id='AddActivities']")
	private WebElement activitytextfield;
	
	@FindBy(xpath = "//p[contains(text(),'Activities name should be less than 45.')]")
	private WebElement activitytext;
	
	/*
	 * 73 testcase webelements
	 */
	
	
	@FindBy(xpath = "//a[contains(text(),'Personalized Media')]")
	private WebElement musicpage;
	
	/*
	 * 74 Testcases webelements
	 */
	@FindBy(xpath = "//span[contains(text(),'Tags')]")
	private WebElement tags;
	@FindBy(xpath = "//span[contains(text(),'Music')]")
	private WebElement music;
	@FindBy(xpath = "//span[contains(text(),'Movie')]")
	private WebElement movie;
	@FindBy(xpath = "//span[contains(text(),'Artist')]")
	private WebElement artist;
	
	/*
	 * 75 testcases
	 */
	@FindBy(xpath = "//input[@id='MediaTags']")
	private WebElement tagsfield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaTags.Add']")
	private WebElement tagsplus;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMusic.add']")
	private WebElement musicplusfield;
	
	@FindBy(xpath = "//input[@id='MediaMovie']")
	private WebElement Moviefield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMovies.add']")
	private WebElement movieplus;
	
	@FindBy(xpath = "//input[@id='MediaArtist']")
	private WebElement artistfield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaArtist.add']")
	private WebElement artistplus;
	
	/*
	 * 76 testcase webelements
	 */
	
	
	@FindBy(xpath = "//button[@id='PatientsList.PersonalizedMusicandMovies-0-7']")
	private WebElement musicplus;
	
	@FindBy(xpath = "//input[@id='MediaTags']")
	private WebElement tagfield;
	
	@FindBy(xpath = "//button[@name='PersonalizedMediaTags.Add']")
	private WebElement tagadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText-0-1']")
	private WebElement tagdata;
	
	
	@FindBy(xpath = "//input[@id='MediaMusic']")
	private WebElement musicfield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMusic.add']")
	private WebElement musicadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText2-0-1']")
	private WebElement musicdata;
	
	
	@FindBy(xpath = "//input[@id='MediaMovie']")
	private WebElement moviefield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMovies.add']")
	private WebElement movieadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText3-0-1']")
	private WebElement moviedata;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaArtist.add']")
	private WebElement artistadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText4-0-1']")
	private WebElement artistdata;
	
	/*
	 * 78 th test case web elements
	 * 
	 */
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete-0-2']")
	private WebElement tagdataremoval;
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete2-0-2']")
	private WebElement musicdataremoval;
	
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete3-0-2']")
	private WebElement moviedataremoval;
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete4-0-2']")
	private WebElement artistdataremoval;
	
	@FindBy(xpath="//button[@class='btn btn-primary pull-right ng-binding']")
	private WebElement alertbox;
	
/*
 * Eva_CCA_Onboard  Patient_072 WebElements
 */
	
	@FindBy(xpath="//input[@id='AddHobbies']")
	private WebElement addhobies;
	
	@FindBy(xpath="//button[@id='AddHobbiesEvent']")
	private WebElement hobbiesaddclick;
	
	@FindBy(xpath="//div[@id='form-group-ActivitesList2-0-1']")
	private WebElement hobbiesdata;
	
	@FindBy(xpath="//div[@class='formio-data-grid ng-scope']/descendant::table[@class='table datagrid-table']/descendant::tr[@class='ng-scope']")
	List<WebElement> activitydata;
	
	
	@FindBy(xpath="//button[@id='AddActivitiesEvent']")
	private WebElement activityclick;
	
	@FindBy(xpath="//input[@id='AddInterests']")
	private WebElement addinterest;
	@FindBy(xpath="//button[@id='AddInterestsEvent']")
	private WebElement addinteresrclick;
	@FindBy(xpath="//div[@id='form-group-ActivitesList3-0-1']")
	private WebElement addinterestdata;
	
	
	/*
	 * Eva_CCA_Onboard  Patient_230 testcase web elements
	 */
	@FindBy(xpath="//button[@name='PatientsList.Events-0-10']")
	private WebElement eventbuttonclick;
	@FindBy(xpath="//small[@class='cal-events-num badge badge-important pull-left ng-binding']")
	private WebElement eventshowlick;
	@FindBy(xpath="//div[@class='cal-slide-box ng-scope in collapse']/descendant::ul/descendant::a/span[contains(text(),'New Event')]/../following-sibling::a/i[@class='glyphicon glyphicon-edit']")
	private WebElement eventedit;
	
	@FindBy(xpath="//input[@class='form-control ng-pristine ng-valid ng-not-empty ng-touched']")
	private WebElement eventitle;
	@FindBy(xpath="//button[@class='componentDialogButton pull-right btn btn-success theme-color ng-binding']")
	private WebElement savebtn;
	
	@FindBy(xpath="//span[@class='ng-binding']")
	private WebElement spantext;
	
	
	/*
	 * 
	 *  verifying On board page is displaying or not
	 * first Testcase
	 */
		public void verifyOnboardPatientTab() throws Throwable {
		
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardtab");
			String exptitle="Onboard Patient - Eva";
			WebActionUtil.verifyTheTitle(exptitle);
			String data=WebActionUtil.getText(gettext, "getttext");
			WebActionUtil.sleep(2);
			if(data.equals("Onboard Patient"))
			{  WebActionUtil.logger.info("stay in Onboard Patient page");
				
			}else
			{	WebActionUtil.logger.info(" not in Onboard Patient page");	
				
			}
		}
		
		/* 
		 * Verifying On board page fields i.e Addnew button ,search field and patient grid
		 *2nd testcase method
		 */
		
		public void verifyTheOnboardTabFields() throws Throwable
		{	 
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardtab");
			WebActionUtil.sleep(3);
			
			/*
			 * fetching all table header data and adding in to the list
			 */
			
				for(int i=2;i<=12;i++)
				{
					 str= list.get(i).getText();
					
					
				}
				
			
			
			boolean add=WebActionUtil.isElementDisplayedOrNot(addNew);
			
			boolean serachfield1=WebActionUtil.isElementDisplayedOrNot(serachfield);
			
			boolean searchbtn1=WebActionUtil.isElementDisplayedOrNot(searchbtn);
			
			/*
			 * validating the result
			 */
			
			if(add==true && serachfield1==true && searchbtn1==true && str.contains(str))
			{
				
				WebActionUtil.logger.info("all fields are displayed");
				
			}
			else
			{
				WebActionUtil.logger.info("all fields are not  displayed");
			}
		}
		
		/*
		 * Verifying the addnew page navigating
		 * third testcase
		 * 
		 */
		public void addNewButton() throws Throwable {
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboard element");
			WebActionUtil.clickOnElementUsingJS(addNew, "addnew");
			boolean Fname=WebActionUtil.isElementDisplayedOrNot(fName);
			boolean lastname=WebActionUtil.isElementDisplayedOrNot(lName);
			
			if(Fname==true && lastname==true)
			{
				WebActionUtil.logger.info("Entering in to the Add new page");
			}else
			{
				WebActionUtil.logger.info("not entering  in to the Add new page");
			}
		}

		/*
		 *Verifying the all the fields are  displayed in addnew patient page or not
		 *4th testcase
		*/
		public void addNewFieldsChecking() throws Throwable
		{	
			
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboard");
			WebActionUtil.clickOnElementUsingJS(addNew, "addnew");
						
			boolean First=WebActionUtil.isElementDisplayedOrNot(firstname);
			boolean Last=WebActionUtil.isElementDisplayedOrNot(lastname);
			
			boolean Email=WebActionUtil.isElementDisplayedOrNot(email);
			
			boolean Gender=WebActionUtil.isElementDisplayedOrNot(gender);
			
			boolean Dob=WebActionUtil.isElementDisplayedOrNot(dob);
			
			boolean Phone=WebActionUtil.isElementDisplayedOrNot(phonenum);
			
			boolean Ephone=WebActionUtil.isElementDisplayedOrNot(ephonenum);
			boolean Econtact=WebActionUtil.isElementDisplayedOrNot(econtact);
			boolean SelectCare=WebActionUtil.isElementDisplayedOrNot(selectcaregiver);
			
			
			WebActionUtil.scrollToElement(cancelbtn, "cancelbtn");
			
			//String data11=WebActionUtil.getText(cancelbtn, "cancelbtn");
			
			
			boolean cancel=WebActionUtil.isElementDisplayedOrNot(cancelbtn);
			
		
			boolean submit=WebActionUtil.isElementDisplayedOrNot(cancelbtn);
			
			
			
			if(First==true && Last==true && Email==true && Gender==true && Dob==true && Phone==true && Ephone==true 
					&& Econtact==true && SelectCare==true && cancel==true && submit==true)
			{
				WebActionUtil.logger.info("Add patient page cotains Firstname,Lastname,Email,Gender,"
						+ "Date of Birth,Phonenumber,Emergency Phone number,Emegency contact number,"
						+ "Status,select caregiver check box,submit and cancel button are displayed.");
			}
			
			else {
				WebActionUtil.logger.info("Some problem is displayed the elements");
			}
			
		}
		
		/* 
		 * Verifying first name field accept special characters,alphabets and numeric
		 * fisth testcase
		*/
		
			public void checkingFirstnameField(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
				WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
				WebActionUtil.sleep(3);
				WebActionUtil.clickOnElementUsingJS(addNew, "add");
				WebActionUtil.sleep(3);
				WebActionUtil.typeText(fName, firstname, "Firstname");
				WebActionUtil.typeText(lName, lastname, "Lastname");
				WebActionUtil.typeText(eMail, email, "Email");
				
				
				String date="September 2019";
				String expdate="02";
				WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
				while(true)
				{	Thread.sleep(100);
					 String monthdata= monthclick.getText();
					
					if(monthdata.equals(date))
					{
						break;
					}else
					{
						WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
					}
				}
				
				driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
				WebActionUtil.clickOnElementUsingJS(gender1, "gender");
				WebActionUtil.sleep(3);
				WebActionUtil.clickOnElementUsingJS(gender, "gender");
			
				
				WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
				WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
				WebActionUtil.typeText(eContactName, econtact, "Econtact");
				WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
				WebActionUtil.sleep(5);
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				WebActionUtil.clickOnElementUsingJS(submit, "submit");
				WebActionUtil.sleep(5);
				String alertdata=driver.switchTo().alert().getText();
				driver.switchTo().alert().accept();
				if(alertdata.contains("Patient Created Successfully"))
				{
					WebActionUtil.logger.info("First Name field should accept alphabets, Numbers, Special characters");
					
				}
			else
				{
					WebActionUtil.logger.info("First Name field should not accept alphabets, Numbers, Special characters");
				}
				WebActionUtil.sleep(2);
				
			}
			
			/* 
			 * Verifying first name field is manditary or not.
			 * 6th testcase
			 */
				public void verifyMandatoryFirstName(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(addNew, "add");
					WebActionUtil.sleep(3);
					/*Shutterbug.shootElement(driver, firstname1).withName("star_beforeClicking").save();
					Snapshot image1 = Shutterbug.shootElement(driver, firstname1).withName("star_beforeClicking");*/
					WebActionUtil.sleep(3);
					WebActionUtil.typeText(fName, firstname, "Firstname");
					WebActionUtil.typeText(lName, lastname, "Lastname");
					WebActionUtil.typeText(eMail, email, "Email");
					
					String date="September 2019";
					String expdate="02";
					WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
					
					while(true)
					{	Thread.sleep(100);
						 String monthdata= monthclick.getText();
						
						if(monthdata.equals(date))
						{
							break;
						}else
						{
							WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
						}
					}
					
					driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
					WebActionUtil.clickOnElementUsingJS(gender1, "gender");
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(gender, "gender");
				
					
					WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
					WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
					WebActionUtil.typeText(eContactName, econtact, "Econtact");
					WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
					WebActionUtil.sleep(5);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					WebActionUtil.sleep(3);
					WebActionUtil.scrollToElement(fName, "element");
					WebActionUtil.sleep(3);
					
					/*Shutterbug.shootElement(driver, firstname1).withName("star_afterClicking").save();
					WebActionUtil.sleep(5);
					Snapshot image2 = Shutterbug.shootElement(driver, firstname1).withName("star_afterClicking");
					assertEquals(image2,image1);*/
					
					String firstnamemandary=fnameasstric.getAttribute("ng-class");
					
					
					if(firstnamemandary.contains("{'field-required': isRequired(component)}"))
					{
						WebActionUtil.logger.info("mandatory symbol (*) is displaying to the user");
					}else
					{
						WebActionUtil.logger.info("mandatory symbol (*) is not displaying to the user");
					}
				}	
			
		/*
		 * 7th testcase
		 * verifying submitton is enabled or not
		 */
				public void submitButtonValidation(String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(addNew, "add");
					WebActionUtil.sleep(3);
					//WebActionUtil.typeText(fName, firstname, "Firstname");
					WebActionUtil.typeText(lName, lastname, "Lastname");
					WebActionUtil.typeText(eMail, email, "Email");
					
					
					String date="September 2019";
					String expdate="02";
					WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
					while(true)
					{	Thread.sleep(100);
						 String monthdata= monthclick.getText();
						
						if(monthdata.equals(date))
						{
							break;
						}else
						{
							WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
						}
					}
					
					driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
					WebActionUtil.clickOnElementUsingJS(gender1, "gender");
					WebActionUtil.sleep(3);
					WebActionUtil.clickOnElementUsingJS(gender, "gender");
				
					
					WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
					WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
					WebActionUtil.typeText(eContactName, econtact, "Econtact");
					WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
					WebActionUtil.sleep(5);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					WebActionUtil.sleep(2);
					boolean subvalue=submit.isEnabled();
					
					Assert.assertEquals(subvalue, false,"submit not displayed");
					WebActionUtil.logger.info("submit button is prasent");
					//WebActionUtil.clickOnElementUsingJS(submit, "submit");
				}
				
				/* 
				 * Verifying last name field accept special characters,alphabets and numeric
				 * 8th testcase
				 */
					public void checkingLastnameField(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
						WebActionUtil.sleep(3);
						WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
						WebActionUtil.sleep(3);
						WebActionUtil.clickOnElementUsingJS(addNew, "add");
						WebActionUtil.sleep(3);
						WebActionUtil.typeText(fName, firstname, "Firstname");
						WebActionUtil.typeText(lName, lastname, "Lastname");
						WebActionUtil.typeText(eMail, email, "Email");
						
						
						String date="September 2019";
						String expdate="02";
						WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
						while(true)
						{	Thread.sleep(100);
							 String monthdata= monthclick.getText();
							
							if(monthdata.equals(date))
							{
								break;
							}else
							{
								WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
							}
						}
						
						driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
						WebActionUtil.clickOnElementUsingJS(gender1, "gender");
						WebActionUtil.sleep(3);
						WebActionUtil.clickOnElementUsingJS(gender, "gender");
					
						
						WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
						WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
						WebActionUtil.typeText(eContactName, econtact, "Econtact");
						WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
						WebActionUtil.sleep(5);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
						WebActionUtil.clickOnElementUsingJS(submit, "submit");
						WebActionUtil.sleep(5);
						String alertdata=driver.switchTo().alert().getText();
						driver.switchTo().alert().accept();
						
						if(alertdata.contains("Patient Created Successfully"))
						{
							WebActionUtil.logger.info("Last Name field should accept alphabets, Numbers, Special characters");
							
						}
					else
						{
							WebActionUtil.logger.info("Last Name field should not accept alphabets, Numbers, Special characters");
						}
						WebActionUtil.sleep(2);
						
					}
					
					
					/* 
					 * Verifying last name field is mandatory or not
					 * 9th testcase
					 */
						public void verifyMandataryLastName(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
							WebActionUtil.sleep(3);
							WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
							WebActionUtil.sleep(3);
							WebActionUtil.clickOnElementUsingJS(addNew, "add");
							WebActionUtil.sleep(3);
							WebActionUtil.typeText(fName, firstname, "Firstname");
							WebActionUtil.typeText(lName, lastname, "Lastname");
							WebActionUtil.sleep(3);
							/*Shutterbug.shootElement(driver, email1).withName("BeforeEmail").save();
							WebActionUtil.sleep(3);
							Snapshot image1 = Shutterbug.shootElement(driver, email1).withName("BeforEmail");*/
							WebActionUtil.typeText(eMail, email, "Email");
							WebActionUtil.sleep(3);
							String date="September 2019";
							String expdate="02";
							WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
							while(true)
							{	Thread.sleep(100);
								 String monthdata= monthclick.getText();
								
								if(monthdata.equals(date))
								{
									break;
								}else
								{
									WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
								}
							}
							
							driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
							WebActionUtil.clickOnElementUsingJS(gender1, "gender");
							WebActionUtil.sleep(3);
							WebActionUtil.clickOnElementUsingJS(gender, "gender");
						
							
							WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
							WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
							WebActionUtil.typeText(eContactName, econtact, "Econtact");
							WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
							WebActionUtil.sleep(5);
							driver.switchTo().activeElement().sendKeys(Keys.TAB);
							WebActionUtil.sleep(3);
							WebActionUtil.scrollToElement(lName, "lastname");
							WebActionUtil.sleep(3);
							
							/*Shutterbug.shootElement(driver, email1).withName("afterEmail").save();
							WebActionUtil.sleep(3);
							Snapshot image2 = Shutterbug.shootElement(driver, email1).withName("afterEmail");
							assertEquals(image1, image2);*/
							
							String lastnamestrick1=lastnamestrick.getAttribute("ng-class");
							
							if(lastnamestrick1.contains("{'field-required': isRequired(component)}"))
							{
								WebActionUtil.logger.info("mandatory symbol of lastname (*) is displaying to the user");
							}else
							{
								WebActionUtil.logger.info("mandatory symbol of lastname (*) is not displaying to the user");
							}
							
						}				
						/*
						 * Validating Last name is mandatory or not.
						 * 10th testcase
						 */
						public void validatingLastName(String firstname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
									
							WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
							WebActionUtil.sleep(3);
							WebActionUtil.clickOnElementUsingJS(addNew, "add");
							WebActionUtil.sleep(3);
							WebActionUtil.typeText(fName, firstname, "Firstname");
							WebActionUtil.typeText(eMail, email, "Email");
							
							String date="September 2019";
							String expdate="02";
							
							WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
							while(true)
							{	Thread.sleep(100);
								 String monthdata= monthclick.getText();
								
								if(monthdata.equals(date))
								{
									break;
								}else
								{
									WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
								}
							}
							
							driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
							WebActionUtil.clickOnElementUsingJS(gender1, "gender");
							WebActionUtil.sleep(3);
							WebActionUtil.clickOnElementUsingJS(gender, "gender");
							WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
							WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
							WebActionUtil.typeText(eContactName, econtact, "Econtact");
							WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
							WebActionUtil.sleep(5);
							driver.switchTo().activeElement().sendKeys(Keys.TAB);
							WebActionUtil.sleep(2);
							boolean subvalue=submit.isEnabled();
							
							Assert.assertEquals(subvalue, false,"submit not displayed");
							
							WebActionUtil.logger.info("Lastname is mandatory");
							
						}	
						/* 
						 * Verifying email is format e.g abc@c.com
						 * 11 th testcase
						 */
							public void checkingEmailField(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
								WebActionUtil.sleep(3);
								WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
								WebActionUtil.sleep(3);
								WebActionUtil.clickOnElementUsingJS(addNew, "add");
								WebActionUtil.sleep(3);
								WebActionUtil.typeText(fName, firstname, "Firstname");
								WebActionUtil.typeText(lName, lastname, "Lastname");
								WebActionUtil.typeText(eMail, email, "Email");
								
								
								String date="September 2019";
								String expdate="02";
								WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
								while(true)
								{	Thread.sleep(100);
									 String monthdata= monthclick.getText();
									
									if(monthdata.equals(date))
									{
										break;
									}else
									{
										WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
									}
								}
								
								driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
								WebActionUtil.clickOnElementUsingJS(gender1, "gender");
								WebActionUtil.sleep(3);
								WebActionUtil.clickOnElementUsingJS(gender, "gender");
							
								
								WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
								WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
								WebActionUtil.typeText(eContactName, econtact, "Econtact");
								WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
								WebActionUtil.sleep(5);
								driver.switchTo().activeElement().sendKeys(Keys.TAB);
								WebActionUtil.clickOnElementUsingJS(submit, "submit");
								WebActionUtil.sleep(5);
								String alertdata=driver.switchTo().alert().getText();
								driver.switchTo().alert().accept();
								
								if(alertdata.contains("Patient Created Successfully"))
								{
									WebActionUtil.logger.info("Email field should accept abc@c.com");
									
								}
							else
								{
									WebActionUtil.logger.info("Email field not accept abc@c.com");
								}
								WebActionUtil.sleep(2);
								
							}	
							/* 
							 * Verifying email field is mandatory or not
							 * 12th test case webelement
							 */
								public void verifyMandataryEmail(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
									WebActionUtil.sleep(3);
									WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
									WebActionUtil.sleep(3);
									WebActionUtil.clickOnElementUsingJS(addNew, "add");
									WebActionUtil.sleep(3);
									WebActionUtil.typeText(fName, firstname, "Firstname");
									WebActionUtil.typeText(lName, lastname, "Lastname");
									WebActionUtil.sleep(3);
									/*Shutterbug.shootElement(driver, email1).withName("BeforeEmail").save();
									WebActionUtil.sleep(3);
									Snapshot image1 = Shutterbug.shootElement(driver, email1).withName("BeforEmail");*/
									WebActionUtil.typeText(eMail, email, "Email");
									WebActionUtil.sleep(3);
									String date="September 2019";
									String expdate="02";
									WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
									while(true)
									{	Thread.sleep(100);
										 String monthdata= monthclick.getText();
										
										if(monthdata.equals(date))
										{
											break;
										}else
										{
											WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
										}
									}
									
									driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
									WebActionUtil.clickOnElementUsingJS(gender1, "gender");
									WebActionUtil.sleep(3);
									WebActionUtil.clickOnElementUsingJS(gender, "gender");
								
									
									WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
									WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
									WebActionUtil.typeText(eContactName, econtact, "Econtact");
									WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
									WebActionUtil.sleep(5);
									driver.switchTo().activeElement().sendKeys(Keys.TAB);
									WebActionUtil.sleep(3);
									WebActionUtil.scrollToElement(eMail, "element");
									WebActionUtil.sleep(3);
									
									/*Shutterbug.shootElement(driver, email1).withName("afterEmail").save();
									WebActionUtil.sleep(3);
									Snapshot image2 = Shutterbug.shootElement(driver, email1).withName("afterEmail");
									assertEquals(image1, image2);*/
									
									String emailastrick1=emailastrick.getAttribute("ng-class");
									
									if(emailastrick1.contains("{'field-required': isRequired(component)}"))
									{
										WebActionUtil.logger.info("mandatory symbol of Email (*) is displaying to the user");
									}else
									{
										WebActionUtil.logger.info("mandatory symbol of Email (*) is not displaying to the user");
									}
									
								}
								/*
								 * Email validation
								 * 13 th test case
								 */
								public void emailFormateValidation() throws Throwable
								{
									WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
									WebActionUtil.sleep(3);
									WebActionUtil.clickOnElementUsingJS(addNew, "add");
									WebActionUtil.sleep(3);
									
									String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.emaildatapath, "Sheet1", "TEST_CASE_NO");
										int i=1; 
									while(i<=4)
									{
										String sdata2 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.emaildatapath, "Sheet1");
									 
										WebActionUtil.typeText(eMail, sdata2, "Email");
										driver.switchTo().activeElement().sendKeys(Keys.TAB);
										WebActionUtil.sleep(2);
										try {
												boolean data=WebActionUtil.isElementDisplayedOrNot(eMailtext);
										
											if(data==true)
											{  
												eMail.clear();
												i++;	
											}else
											{
											WebActionUtil.logger.info("Email suceesfully verified all conditions");
											}
										}catch(AssertionError e)
										{
											WebActionUtil.logger.info("Email suceesfully verified all conditions");
										}
									}
								}
								/* 
								 * Verifying invalid email  format e.g abc@)
								 * 14 th testcase
								 *
								 */
								
									public void emailValidationMessage(String email) throws Throwable {
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "add");
										WebActionUtil.sleep(3);
										WebActionUtil.typeText(eMail, email, "Email");
										driver.switchTo().activeElement().sendKeys(Keys.TAB);
										WebActionUtil.sleep(3);
										String emailvaidation=WebActionUtil.getText(emailvaldationmsg, "emial validation message will diaplay");
										
										WebActionUtil.logger.info(emailvaidation);
									
									
										
										if(emailvaidation.contains("Please Enter Email"))
										{
											WebActionUtil.logger.info("Invalid Email id formate is not allowing to the Email formate");
											
										}
									else
										{
											WebActionUtil.logger.info("Invalid Email id formate is  allowing to the Email formate");
										}
										WebActionUtil.sleep(1);
										
									}
									/* 
									 * 
									 * Validating all Gender fields are displaying or not
									 * 15 th test case
									 */
									
									public void checkingGenderFiled() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "add");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(gender1, "gender");
										WebActionUtil.sleep(1);
										boolean malefield=WebActionUtil.isElementDisplayedOrNot(gendermale);
										
										boolean femalefield=WebActionUtil.isElementDisplayedOrNot(genderfemale);
									
										
										if(malefield==true && femalefield==true)
										{
											WebActionUtil.logger.info("All Gender fields are displaying");
											
										}
										else
										{
											WebActionUtil.logger.info("All Gender fields are  not displaying");
										}
										
									}	
									/* 
									 * verifying user can able to select the gender fields in drop down
									 * 16 th test case
									 */
									
									public void genderFiledsSelecting() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
										WebActionUtil.sleep(2);
										WebActionUtil.clickOnElementUsingJS(gendermale, "click on Male value");
										WebActionUtil.sleep(3);
										
										
										WebActionUtil.logger.info("user can able to select the Gender in drop down");
										try {
										WebActionUtil.clearAndTypeText(gender1, "Trans", "user can not select invalid Gender");
										}catch(AssertionError e){
											WebActionUtil.logger.info("user can't select the invalid gender data");
										}
										
										WebActionUtil.sleep(1);
										
									}
									/* 
									 * verifying user can able to see either selected data is displayed or not in the gender field
									 * 17 th test case
									 */
									public void checkingDataDisplatedOrNot() throws Throwable {
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
										WebActionUtil.sleep(2);
										WebActionUtil.clickOnElementUsingJS(genderfemale, "click on feMale value");
										WebActionUtil.sleep(3);
										
										
										try {
											WebActionUtil.verifyElementText(femalevalue, "Female");
											WebActionUtil.logger.info("Entered text is  displayed");
										}catch(AssertionError e)
										{
											WebActionUtil.logger.info("Entered text is not displayed");
										}
										
									}
									/*
									 * checking multiple data in gender field
									 * 18 th test case
									 */
									public void checkMultipleDataInGender() throws Throwable {
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
										WebActionUtil.sleep(2);
										WebActionUtil.clickOnElementUsingJS(genderfemale, "click on feMale value");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
										WebActionUtil.sleep(2);
										WebActionUtil.clickOnElementUsingJS(gendermale, "clicking on male value");
										WebActionUtil.sleep(3);
										try {
												WebActionUtil.verifyElementText(malevalue, "Male");
												WebActionUtil.logger.info("Female value is replaced with male hence user can not enter multiple value");
											}catch(AssertionError e)
											{
												WebActionUtil.logger.info("Gender field is  allowing entering multipledata");
											}
									}	
									/*
									 * date filed is displayed or not
									 * 19 th testcase
									 */
									public void dateFiledDisplay() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on bordpatient");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
										WebActionUtil.sleep(3);
										boolean date=WebActionUtil.isElementDisplayedOrNot(dateicon);
										
										if(date=true)
										{
											WebActionUtil.logger.info("Date field is Present");
											WebActionUtil.clickOnElementUsingJS(dateicon, "clicking on date");
											WebActionUtil.sleep(1);
											boolean calender=WebActionUtil.isElementDisplayedOrNot(opencalender);
											if(calender=true) {
												
												String calender1=WebActionUtil.getText(opencalender, "callendr is present");
												WebActionUtil.logger.info(calender1+"calender is opened");
											}
											else
											{
											WebActionUtil.logger.info("date is not prasent");
											}
										}
									}
									public void personalActivityValidation() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(pinterest, "clicking on personal interest");
										WebActionUtil.sleep(2);
										WebActionUtil.typeText(activitytextfield, "caregivercaregivercaregivercaregivercaregivercaregiver", "testdatamax45characters");
										WebActionUtil.sleep(1);
										driver.switchTo().activeElement().sendKeys(Keys.TAB);
										//String data=WebActionUtil.getText(activitytext, "getting text");
										//System.out.println(data);
										try {
												WebActionUtil.verifyElementText(activitytext, "Activities name should be less than 45.");
												WebActionUtil.logger.info("Personalise text filed only accept max45 characters");
											}catch(AssertionError e)
											{
												WebActionUtil.logger.info("Personalise text filed any lenght of characters");
											}
									}
									/*
									 * user can navigating music page
									 * 73 th test case
									 */
									public void musicPageValidation() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(3);
										//String musicpagedata=WebActionUtil.getText(musicpage, "validationmusic");
										
										
										try {
											WebActionUtil.verifyElementText(musicpage, "Personalized Media");
											WebActionUtil.logger.info("user is in navigated to Music and Movies page ");
										}catch(AssertionError e){
											WebActionUtil.logger.info("user is not in navigated to Music and Movies page ");
										}
									}
									/*
									 * verifying music page fields
									 * 74 th test case
									 */
									
									public void verifyMusicPageFields() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(3);
										
										boolean tags1=WebActionUtil.isElementDisplayedOrNot(tags);
										
										boolean music1=WebActionUtil.isElementDisplayedOrNot(music);
										
										boolean movie1=WebActionUtil.isElementDisplayedOrNot(movie);
										
										boolean artist1=WebActionUtil.isElementDisplayedOrNot(artist);
										
										
										/*
										 * validating the result
										 */
										
										if(tags1==true && music1==true && movie1==true && artist1==true)
										{
											
											WebActionUtil.logger.info("tags,music,movie,artist fields are displaying");
											
										}
										else
										{
											WebActionUtil.logger.info("tags,music,movie,artist fields are not displaying");
										}
										
									}
									/*
									 * verifying music page
									 * 75 th test script
									 */
									
									public void verifyMusicPage() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(3);
										
										boolean tagsfield1=WebActionUtil.isElementDisplayedOrNot(tagsfield);
										
										boolean tagsplu1s=WebActionUtil.isElementDisplayedOrNot(tagsplus);
										
										boolean musicfield1=WebActionUtil.isElementDisplayedOrNot(musicfield);
										
										boolean movieplus1=WebActionUtil.isElementDisplayedOrNot(movieplus);
										
										boolean artistfield1=WebActionUtil.isElementDisplayedOrNot(artistfield);
										
										boolean artistplus1=WebActionUtil.isElementDisplayedOrNot(artistplus);
									
										
										/*
										 * validating the result
										 */
										
										if(tagsfield1==true && tagsplu1s==true && musicfield1==true 
												&& movieplus1==true && artistfield1==true && artistplus1==true)
										{
											
											WebActionUtil.logger.info("tags,music,movie,artist fields and add button  are displaying");
											
										}
										else
										{
											WebActionUtil.logger.info("tags,music,movie,artist fields and add button are not displaying");
										}
									}
									
									
									/*
									 * verifying user can add data
									 * 76 th test case
									 */
									public void validateUserCanAddData() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(3);
										WebActionUtil.typeText(tagfield, "youtube", "adding data on tagfield");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(tagadd, "click on add");
										/*String data=tagdata.getAttribute("title");
										WebActionUtil.sleep(3);
										
										System.out.println(data);*/
										boolean data=WebActionUtil.isElementDisplayedOrNot(tagdata);
										
										WebActionUtil.typeText(musicfield, "Melody", "adding data on music");
										WebActionUtil.sleep(3);
										
										WebActionUtil.clickOnElement(musicadd, "click on add");
										
										
										WebActionUtil.sleep(2);
										
										/*String data1=musicdata.getAttribute("title");
										
										System.out.println(data1);*/
										
										boolean data1=WebActionUtil.isElementDisplayedOrNot(musicdata);
										
										
										WebActionUtil.typeText(moviefield, "Bahubali", "adding data on Movie");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(movieadd, "click on add");
										
										WebActionUtil.sleep(3);
										/*String data3=moviedata.getAttribute("title");
										
										System.out.println(data3);*/
										
										boolean data2=WebActionUtil.isElementDisplayedOrNot(moviedata);
										
										
										
										WebActionUtil.typeText(artistfield, "Praphas", "adding data on Artist");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(artistadd, "click on add");
										
										WebActionUtil.sleep(3);
										/*String data4=artistdata.getAttribute("title");
										
										System.out.println(data4);*/
										
										boolean data3=WebActionUtil.isElementDisplayedOrNot(artistdata);
										
										
										if(data==true && data1==true && data2==true && data3==true )
										{
											WebActionUtil.logger.info("User should be allowed to add data's into  Tags/Music/Movie/Artist Portlets");
										}else
										{
											WebActionUtil.logger.info("User not allowed to add data's into  Tags/Music/Movie/Artist Portlets");
										}
									}
									/*
									 * verify the verify thr data edit
									 * 77 th testcase
									 */
									public void verifyDataEdit() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(3);
										
										WebActionUtil.typeText(tagfield, "youtube", "adding data on tagfield");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(tagadd, "click on add");
										String data=tagdata.getAttribute("read-only");
										
										
										
										WebActionUtil.typeText(musicfield, "Melody", "adding data on music");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(musicadd, "click on add");
										WebActionUtil.sleep(2);
										String data1=musicdata.getAttribute("read-only");
										
										
										
										WebActionUtil.typeText(moviefield, "Bahubali", "adding data on Movie");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(movieadd, "click on add");
										WebActionUtil.sleep(3);
										String data2=moviedata.getAttribute("read-only");
										
										
										WebActionUtil.typeText(artistfield, "Praphas", "adding data on Artist");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(artistadd, "click on add");
										WebActionUtil.sleep(3);
										String data3=artistdata.getAttribute("read-only");
										
										if(data.contains("isDisabled(col, row)") && data1.contains("isDisabled(col, row)")
												&& data2.contains("isDisabled(col, row)") && data3.contains("isDisabled(col, row)") )
										{
											WebActionUtil.logger.info("user can't edit the data and he can add the data & delete the data");
										}
										else
										{
											WebActionUtil.logger.info("user can edit the data and he can add the data & delete the data");
										}
									}
									/*
									 * removing add data checking
									 * 78 th testcase
									 */
									
									public void removeingAddedData() throws Throwable
									{
										WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
										WebActionUtil.sleep(1);
										WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
										WebActionUtil.sleep(1);
										WebActionUtil.typeText(tagfield, "youtube", "adding data on tagfield");
										WebActionUtil.sleep(1);
										WebActionUtil.clickOnElement(tagadd, "click on add");
										boolean data=WebActionUtil.isElementDisplayedOrNot(tagdata);
										
									
										
										WebActionUtil.typeText(musicfield, "Melody", "adding data on music");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(musicadd, "click on add");
										WebActionUtil.sleep(2);
										boolean data1=WebActionUtil.isElementDisplayedOrNot(musicdata);
										
										WebActionUtil.typeText(moviefield, "Bahubali", "adding data on Movie");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(movieadd, "click on add");
										WebActionUtil.sleep(3);
										boolean data2=WebActionUtil.isElementDisplayedOrNot(moviedata);
										
										
										WebActionUtil.typeText(artistfield, "Praphas", "adding data on Artist");
										WebActionUtil.sleep(3);
										WebActionUtil.clickOnElement(artistadd, "click on add");
										WebActionUtil.sleep(3);
										
										boolean data3=WebActionUtil.isElementDisplayedOrNot(artistdata);
										
										if(data==true && data1==true && data2==true && data3==true )
										{
											WebActionUtil.logger.info("User should be allowed to add data's into  Tags/Music/Movie/Artist Portlets");
										}else
										{
											WebActionUtil.logger.info("User not allowed to add data's into  Tags/Music/Movie/Artist Portlets");
										}
											
											WebActionUtil.clickOnElement(tagdataremoval, "click on add");
											WebActionUtil.sleep(3);
											WebActionUtil.clickOnElement(alertbox, "click on add");
											WebActionUtil.sleep(3);
										try {
											boolean datar11=WebActionUtil.isElementDisplayedOrNot(tagdata);
											WebActionUtil.logger.info("user can not delete the tag data");
										}catch(Exception e) 
										{
											WebActionUtil.logger.info("user can delete the tag data");
											
										}
									
											WebActionUtil.clickOnElement(musicdataremoval, "click on add");
											WebActionUtil.sleep(3);
											
											WebActionUtil.clickOnElement(alertbox, "click on add");
										try {
												boolean data1r=WebActionUtil.isElementDisplayedOrNot(musicdata);
										
												WebActionUtil.logger.info("user can not delete the musicdata");
										}catch(Exception e) 
										{
											WebActionUtil.logger.info("user can  delete the musicdata");
										}
										
											WebActionUtil.clickOnElement(moviedataremoval, "click on add");
											WebActionUtil.sleep(3);
											
											WebActionUtil.clickOnElement(alertbox, "click on add");
										try {
												boolean data2r=WebActionUtil.isElementDisplayedOrNot(moviedata);
												WebActionUtil.logger.info("user can not delete the moviedata");
											}catch(Exception e)
											{
												WebActionUtil.logger.info("user can  delete the moviedata");
											}
											WebActionUtil.clickOnElement(artistdataremoval, "click on add");
											WebActionUtil.sleep(3);
											
											WebActionUtil.clickOnElement(alertbox, "click on add");
										
										try {
											boolean data3r=WebActionUtil.isElementDisplayedOrNot(artistdata);
											WebActionUtil.logger.info("user can not delete the artistdata");
										}catch(Exception e) {
											WebActionUtil.logger.info("user can  delete the artistdata");
										}
										
									}
	public void userAddTenInterest() throws Throwable
	{	
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "click onboardpt");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(pinterest, "personalinteresr");
		WebActionUtil.sleep(2);
		for(int i=0;i<=10;i++)
		{
			WebActionUtil.typeText(activitytextfield,RandomStringUtils.randomAlphabetic(5) , "adding text");
			WebActionUtil.clickOnElement(activityclick, "clicking on element");
			WebActionUtil.sleep(2);
			
		}
		WebActionUtil.logger.info("user can able to add 10 activities");
		
		for(int i=0;i<=10;i++)
		{
			WebActionUtil.typeText(addhobies, RandomStringUtils.randomAlphabetic(5), "adding hobies");
			WebActionUtil.clickOnElement(hobbiesaddclick, "adding hobies");
			WebActionUtil.sleep(2);
			
		}
		WebActionUtil.logger.info("user can able to add 10 hobies");	
		for(int i=0;i<=10;i++)
		{
			WebActionUtil.typeText(addinterest, RandomStringUtils.randomAlphabetic(5), "adding  interest");
			WebActionUtil.clickOnElement(addinteresrclick, "adding click");
			WebActionUtil.sleep(2);
		}
		WebActionUtil.logger.info("user can able to add 10 interest");
		if(activitydata.size()==40);
		{
			WebActionUtil.logger.info("user can able to add 10 activities in all fields");
		}
	}
public void verifyEventModification() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(eventbuttonclick, "clicking on event");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(eventshowlick, "event clicking");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(eventedit, "edit the event");
	WebActionUtil.sleep(2);
	WebActionUtil.clearAndTypeText(eventitle, "New Event 5", "editing the event");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(submit, "submit the button");
	WebActionUtil.sleep(2);
	String eventdata=WebActionUtil.getText(spantext, "getting text");
	System.out.println(eventdata);
}
}



