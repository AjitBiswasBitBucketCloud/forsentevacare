package com.forsenteva.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.library.WebActionUtil;

public class HomePage extends BasePage{
	static WebDriver driver;
	String First_Name;
	 String Last_Name;
	 String Email_Id;
	 String Phone_No;
	 String Emergency_Name;
	 String Emergency_No;
	 String Gender_Name;
	 
	public ExcelDataProvider excelLibrary = new ExcelDataProvider();
	
	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	// //div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table
	@FindBy(xpath = "//div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table")
	private WebElement patientTable;
	
	//  //div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table/tbody/tr
	@FindBy(xpath = "//div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table")
	private List<WebElement> patientTableRows;
	
	// //div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table/tbody/tr/th
	@FindBy(xpath = "//div[@class='form-group form-field-type-datagrid formio-component-PatientsList patientListGrisCls']/div/div[2]/div/table/tbody/tr/th")
	private List<WebElement> patientTableColumns;
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	@FindBy(xpath = "//span[contains(text(),'Add New')]")
	private WebElement addNew;
	@FindBy(xpath = "//input[@name='FirstName']")
	private WebElement fName;
	@FindBy(xpath = "//input[@name='LastName']")
	private WebElement lName;
	@FindBy(xpath = "//input[@id='Email']")
	private WebElement eMail;
	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement ePhoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactName']")
	private WebElement eContactName;
	
	
	@FindBy(xpath = "//input[@id='DateOfBirth']")
	private WebElement dobsending;
	
	@FindBy(xpath="//div[@id='Gender']/div[1]/span/i")
	private WebElement gender1;
	
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-1']/span/formio-select-item/span")
	private WebElement gender;
	
	
	@FindBy(xpath = "//button[@class='btn btn-default']")
	private WebElement clalender;
	
	@FindBy(xpath="//strong[@class='ng-binding']")
	private WebElement monthclick;
	
	@FindBy(xpath="//strong[@class='ng-binding']")
	private WebElement yearclick;
	
	@FindBy(xpath="//span[@class='ng-binding'][0]")
	private WebElement moving;
	
	@FindBy(xpath="//span[@class='ng-binding'][18]")
	private WebElement moving1;
	
	
	@FindBy(xpath="//span[contains(text(),'next')]")
	private WebElement next;
	
	@FindBy(xpath="//input[@name='CareGiver-115']")
	private WebElement caregiver;
	
	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
	private WebElement submit;
	
//	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
//	private WebElement submi;
//		
	public void getTableRowsUsingColumn(String columnName) {
		for(WebElement e:patientTableColumns) {
			String ActualColumnName = e.getText();
			if(columnName.equalsIgnoreCase(ActualColumnName)) {
				
			}
		}
	}
	
	
		// Method to click onBoard patient
	public void clickOnBoardpatient() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "OnBoard Patient");
		WebActionUtil.sleep(2);
	}
	
	public void clickAddNew() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
	}
	
	public void EnterFirstName() throws Throwable {
		WebActionUtil.typeText(fName, First_Name, "Firstname");
	}
	
	
		public void addNewPatient() throws Throwable {
			// fetch a data from excel to create a new patient
			String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");
			for(int i=1; i<sdata1.length; i++)
			 {
				
				 First_Name = ExcelDataProvider.readcolData(sdata1[i], "FName", GenericLib.testDataPath1, "Sheet1");	 
					System.out.println(First_Name);
								
					 Last_Name = ExcelDataProvider.readcolData(sdata1[i], "Lname", GenericLib.testDataPath1, "Sheet1");	 
						System.out.println(Last_Name);
						
						// Gender   ele = Gender_Name
						Gender_Name = ExcelDataProvider.readcolData(sdata1[i], "Gender", GenericLib.testDataPath1, "Sheet1");	 
						System.out.println(Gender_Name);
						
						 Email_Id = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.testDataPath1, "Sheet1");	 
							System.out.println(Email_Id);
							
							 Phone_No = ExcelDataProvider.readcolData(sdata1[i], "phone", GenericLib.testDataPath1, "Sheet1");	 
								System.out.println(Phone_No);
								
								  Emergency_Name = ExcelDataProvider.readcolData(sdata1[i], "Ephone", GenericLib.testDataPath1, "Sheet1");	 
									System.out.println(Emergency_Name);
									
									 Emergency_No = ExcelDataProvider.readcolData(sdata1[i], "Econtact", GenericLib.testDataPath1, "Sheet1");	 
										System.out.println(Emergency_No);

										clickOnBoardpatient();
										WebActionUtil.sleep(3);
										clickAddNew();
										WebActionUtil.sleep(3);
										EnterFirstName();
			
										WebActionUtil.typeText(lName, Last_Name, "Lastname");
										WebActionUtil.typeText(eMail, Email_Id, "Email");
										
			//Select select=new Select(driver.findElement(By.xpath("//i[@class='caret pull-right']")));
			//select.deselectByVisibleText("Male");
			
			
			//String date=Excell.getCellData(GenericLib.testdata, "Addpatient", 6, 1);
			/*System.out.println("Date "+date);
			String datarry[]= date.split("\\.");
			
			String day=datarry[0];
			String month=datarry[1];
			String year=datarry[2];
								
			WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
			WebActionUtil.clickOnElementUsingJS(monthclick, "clicking on month");
			WebActionUtil.clickOnElementUsingJS(yearclick, "clicking on year");
			
			String startyear=moving.getText();
			String endyear=moving1.getText();
			
			
				int startyear1 = Integer.parseInt(startyear);
				int endyear1 = Integer.parseInt(endyear);
				int year1 = Integer.parseInt(year);
				int index=endyear1-startyear1;
			
			
			if(startyear1>=year1&&year1<=endyear1)
			{
				String clickin_element="//span[@class='ng-binding']["+index+"]";
				driver.findElement(By.xpath(clickin_element)).click();
			}*/
			
			
			String date="August 2019";
			String expdate="25";
			WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
			if(WebActionUtil.isAlertPresent(driver, 10)) {
				i++;
				String First_Name_1 = ExcelDataProvider.readcolData(sdata1[i], "FName", GenericLib.testDataPath1, "Sheet1");
				System.out.println(First_Name_1);
				
				String Last_Name_1 = ExcelDataProvider.readcolData(sdata1[i], "Lname", GenericLib.testDataPath1, "Sheet1");	 
				System.out.println(Last_Name_1);
				
				WebActionUtil.clearAndTypeText(fName,First_Name_1, "First_Name_1");
				WebActionUtil.clearAndTypeText(lName, Last_Name_1, "Lastname");
				
			}
			
			while(true)
			{	Thread.sleep(100);
				 String monthdata= monthclick.getText();
				 
				 // checking whether date set to current date
				if(monthdata.equals(date))
				{
					break;
				}else
				{
					WebActionUtil.clickOnElementUsingJS(next, "Next Button");
				}
			}
			
			//* set gender field 
			
			//driver.findElement(By.xpath("(//span[@class='ng-binding'])[25]")).click();
			//WebActionUtil.clickOnElementUsingJS(gender1, "gender");
			//WebActionUtil.selectDropdownByValue(gender1, value);
			WebActionUtil.selectDropdownByValue(gender1, Gender_Name);
			WebActionUtil.sleep(3);
			//WebActionUtil.clickOnElementUsingJS(gender, "gender");
			//WebActionUtil.typeText(dobsending, dob, "Phonenumber");
			
			WebActionUtil.typeText(phoneNum, Phone_No, "Phonenumber");
			WebActionUtil.typeText(ePhoneNum, Emergency_No, "Ephonenum");
			WebActionUtil.typeText(eContactName, Emergency_Name , "Econtact");
			WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
			WebActionUtil.sleep(5);
//			WebActionUtil.clickOnElement(submit, "submit");
			WebActionUtil.scrollByPixel(20, 0);
			WebActionUtil.clickOnElementUsingJS(submit, "submit");
			WebActionUtil.sleep(500);
			
		}		
			
			
		}
}

		


