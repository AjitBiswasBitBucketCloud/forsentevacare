package com.forsenteva.web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.WebActionUtil;


public class Login_Page extends BasePage
{

	public Login_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		super(driver);
	}

	@FindBy(xpath = "//input[@type='text']")
	private WebElement emailTxtBx;

	@FindBy(xpath = "//input[@type='password']")
	private WebElement passWordTxtBx;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	private static  WebElement loginBtn;
	
	

	public void loginToApplication(String username,String password) throws Throwable 
	{
		
		WebActionUtil.typeText(emailTxtBx, username, "Email Address Text box");
		WebActionUtil.typeText(passWordTxtBx,password, "Password Text Box");
		//webActionUtil.typeText(emailTxtBx,indexOfUserName, "Email Address Text box");		
		
		//webActionUtil.typeText(passWordTxtBx,indexOfpass, "Password Text Box");
		
		WebActionUtil.clickOnElementUsingJS(loginBtn, "button");
		
		WebActionUtil.waitTillPageLoad(driver, 20);
	}



	public void verifySignOut() {
		// TODO Auto-generated method stub
		
	}


}
