package com.forsenteva.web.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.WebActionUtil;

/**
 * @author Sindhu N
 *
 */
public class OnBoardPatientPage extends BasePage{
	WebDriverWait wait = new WebDriverWait(driver,20);

	public OnBoardPatientPage(WebDriver driver) {
		super(driver);
	}

	/*
	 * 
	 * OnBoard Patient page elements
	 */

	@FindBy(xpath ="//button[@id='PatientsList.PersonalizedMusicandMovies-0-7']")
	private WebElement MusicAndMoviesbutton;

	@FindBy(css = "button[id='PatientsList.PersonalizedMediaLibrary-0-8']")
	private WebElement personalizedLibraryButton;

	@FindBy(css = "button[id*='PatientsList.PersonalInterests-0-6']")
	private WebElement personalInterestButton;

	@FindBy(css = "button[id='PatientsList.Conversation-0-9']")
	private WebElement conversationButton;

	/*Personalized Media page elements
	 * 
	 * 
	 */
	@FindBy(xpath = "//a[text()='Personalized Media']")
	private WebElement pesonalizedMediaTab;

	@FindBy(css = "button[id*='Delete']")
	private List<WebElement> deleteButton;

	@FindBy(css = "input[id='MediaTags']")
	private WebElement tagsTextbox;

	@FindBy(css = "form-group-PersonalizedMediaText-1-1")
	private WebElement tagsTextButton;

	@FindBy(css = "button[id='PersonalizedMediaTags.Add']")
	private WebElement tagsAddButton;

	@FindBy(css = "button[id='PersonalizedMediaDelete-0-2']")
	private WebElement tagsremoveButton;

	@FindBy(css = "input[id='MediaMusic']")
	private WebElement musicTextbox;

	@FindBy(css = "button[id='PersonalizedMediaMusic.add']")
	private WebElement musicAddButton;

	@FindBy(css = "div[id='form-group-PersonalizedMediaText2-0-1']")
	private WebElement musicTextButton;

	@FindBy(css = "div[id='button[id='PersonalizedMediaDelete2-0-2']")
	private WebElement musicRemoveButton;

	@FindBy(css = "div[id='button[id='PersonalizedMediaDelete3-0-2")
	private WebElement movieRemoveButton;
	
	@FindBy(css = "input[id='MediaMovie']")
	private WebElement movieTextbox;

	@FindBy(css = "button[id='PersonalizedMediaMovies.add']")
	private WebElement movieAddButton;

	@FindBy(css = " div[id='form-group-PersonalizedMediaText3-0-1']")
	private WebElement movieTextButton;

	@FindBy(css = "input[id='MediaArtist']")
	private WebElement artistTextbox;

	@FindBy(css = "button[id='PersonalizedMediaArtist.add']")
	private WebElement artistAddButton;

	@FindBy(css = "div[id='div[id='form-group-PersonalizedMediaText4-0-1']")
	private WebElement artistTextButton;
	
	@FindBy(css = "div[id='div[id='PersonalizedMediaDelete4-0-2")
	private WebElement artistRemoveButton;

	/*
	 * Tabs
	 * 
	 */
	@FindBy(xpath = "//a[text()='Personal Interests']")
	private WebElement personalInterestTab;

	@FindBy(xpath = "//a[text()='Conversation']")
	private WebElement conversationTab;

	@FindBy(xpath = "//a[text()='Personalized Media Library']")
	private WebElement pesonalizedMediaLibraryTab;
	
	@FindBy(css = "div[id*='form-group-Name']")
	private WebElement mediaNames;

	/*
	 * 
	 * personalized media library page elements
	 */
	@FindBy(css = "input[id='PersonalizedSearch']")
	private WebElement personalizedSearchTextbox;
	
	@FindBy(css = "button[id='searchButton2']")
	private WebElement personalizedSearchButton;
	
	@FindBy(css = "table[class*='datagrid-table']")
	private WebElement table;
	
	@FindBy(xpath = "//span[text()='All']")
	private WebElement allDropdown;
	
	@FindBy(css = "button[id*='Refresh2']")
	private WebElement Refreshbutton;
	
	/*
	 * 
	 * conversation page elements
	 */
	@FindBy(css = "button[id*='AddNew']")
	private WebElement addNewButton;

	@FindBy(xpath = "//input[@id='PatientName']")
	private WebElement patientName;

	@FindBy(css = "input[id*='QuestionPhrase']")
	private WebElement questionTextbox;

	@FindBy(css = "label[for='Mood']")
	private WebElement moodDropdown;

	@FindBy(css = "div[id='form-group-Subjects']")
	private WebElement subjectDropdown;

	@FindBy(css = "div[id='form-group-AnswerType']")
	private WebElement responseType;

	@FindBy(css = "textarea[id='Answer']")
	private WebElement answerMedia;

	@FindBy(css = "div[class*='ProfileImageContainer']")
	private WebElement uploadImage;

	@FindBy(css = "div[id='form-group-CommunicationForm.Record']")
	private WebElement record;

	@FindBy(css = "input[id='AlertInfo']")
	private WebElement alertCheckbox;

	@FindBy(css = "div[id*='RepeatedMoreThanTimes']")
	private WebElement repeatedMoreThan;

	@FindBy(css = "div[class*='Within']")
	private WebElement within;

	@FindBy(css = "div[class*='Trigger']")
	private WebElement trigger;

	@FindBy(css = "div[class*='Email']")
	private WebElement email;

	@FindBy(css = "button[id='Submit']")
	private WebElement submitButton;

	@FindBy(css = "button[id*='Cancel']")
	private WebElement cancel;

	@FindBy(css = "label[for*='QuestionPhrase']")
	private WebElement phraseLabel;

	@FindBy(xpath = "//span[text()='No Results Found']")
	private WebElement errorMessage;

	@FindBy(css = "input[id='Filter']")
	private WebElement filterTextBox;

	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;

	/*Setters  
	 * 
	 * 
	 */

	/*Method to enter data into tags field
	 * 
	 * 
	 */
	public void enterTags(String data) throws Throwable {
		WebActionUtil.typeText(tagsTextbox, data, "tags");
		WebActionUtil.clickOnElement(tagsAddButton, "tags add");
	}

	/*Method to enter data into music field
	 * 
	 * 
	 */
	public void enterMusic(String data) throws Throwable {
		WebActionUtil.typeText(musicTextbox, data, "music");
		WebActionUtil.clickOnElement(musicAddButton, "music add");
	}

	/*Method to enter data into movie field
	 * 
	 * 
	 */
	public void enterMovie(String data) throws Throwable {
		WebActionUtil.typeText( movieTextbox, data, "movie");
		WebActionUtil.clickOnElement( movieAddButton, "movie add");
	}

	/*Method to enter data into artist field
	 * 
	 * 
	 */
	public void enterArtist(String data) throws Throwable {
		WebActionUtil.typeText( artistTextbox, data, "artist");
		WebActionUtil.clickOnElement( artistAddButton, "artist add");
	}

	/*Getters 
	 * 
	 * 
	 */

	/*Method to get data from tags field
	 * 
	 * 
	 */
	public String getTagText() {
		String title=tagsTextButton.getAttribute("title"); 
		return title;
	}

	/*Method to get data from music field
	 * 
	 * 
	 */
	public String getMusicText() {
		String title= musicTextButton.getAttribute("title"); 
		return title;
	}

	/*Method to get data from movie field
	 * 
	 * 
	 */
	public String getMovieText() {
		String title= movieTextButton.getAttribute("title"); 
		return title;
	}

	/*Method to get data from artist field
	 * 
	 * 
	 */
	public String getArtistText() {
		String title= musicTextButton.getAttribute("title"); 
		return title;
	}

	/*Clicks
	 * 
	 * 
	 */

	/*Method to click on onBoard patient tab      
	 * 
	 * 
	 */
	public void clickOnBoardpatient() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "OnBoard Patient");
		WebActionUtil.sleep(2);
	}

	/*Method to click on add new button
	 * 
	 * 
	 */
	public void clickAddNew() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Add New Button");
	}

	/*Method to click on onBoard patient tab
	 * 
	 * 
	 */
	 public void clickOnBoardpatient1() throws Throwable {
	 WebActionUtil.clickOnElementUsingJS(onboardPatient, "OnBoard Patient");
	 WebActionUtil.sleep(2);
	}
	 
	 /*Method to click on add button under music and movies column
	 * 
	 * 
	 */
	public void clickMusicAndMoviesAdd() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='PatientsList.PersonalizedMusicandMovies-0-7']")));
		//WebActionUtil.clickOnElement(MusicAndMoviesbutton, "clicking on button");
		WebActionUtil.clickOnElementUsingJS(MusicAndMoviesbutton, "button");
	}

	/*Method to click on add button under conversation column
	 * 
	 * 
	 */
	public void clickConnversationAdd() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(conversationButton, "clicking on button");		
	}

	/*Method to click on add button under personal interest column
	 * 
	 * 
	 */
	public void clickPersonalInterestAdd() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(personalInterestButton, "clicking on button");		
	}

	/*Method to click on add button under personalized media column
	 * 
	 * 
	 */
	public void clickPersnalizedMediadAdd() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(personalizedLibraryButton, "clicking on button");		
	}

	/*Method to click refresh button 
	 * 
	 * 
	 */
	public void clickRefresh() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(Refreshbutton, "refresh");
	}
	
	/*Actions
	 * 
	 * 
	 */

	/*Method to remove data under tags field
	 * 
	 * 
	 */
	public void removeTagsData() throws Throwable {
		boolean isDisplayed= tagsremoveButton.isDisplayed();
		if(isDisplayed) {
		WebActionUtil.clickOnElementUsingJS(tagsremoveButton, "tags remove button");
		}
	}
	
	/*Method to remove data under music field
	 * 
	 * 
	 */
	public void removeMusicData() throws Throwable {
		if(musicRemoveButton.isDisplayed()) {
		WebActionUtil.clickOnElement(musicRemoveButton, "music remove button");
		}
	}
	
	/*Method to remove data under movie field
	 * 
	 * 
	 */
	public void removeMovieData() throws Throwable {
		WebActionUtil.clickOnElement(movieRemoveButton, "music remove button");
	}
	
	/*Method to remove data in all portlets      
	 * 
	 * 
	 */
	public void removeDataFromAllPortlets() throws InterruptedException {
		Thread.sleep(1000);
		for(WebElement element :deleteButton) {
			if(element.isDisplayed()) {
				element.click();
			}
		}
	}

	/*Verifications
	 * 
	 * 
	 */

	/*Method to check whether phrase field accepts given test data
	 * 
	 * 
	 */
	public void validatePhrase(String question,String mood, String subject) throws Throwable {
		WebActionUtil.typeText(questionTextbox, question, "");
		WebActionUtil.selectDropdownByValue(moodDropdown, mood);
		WebActionUtil.selectDropdownByValue(subjectDropdown, subject);
		WebActionUtil.isElementEnabled(submitButton);
	}

	/*Method to check phrase field is mandatory field
	 * 
	 * 
	 */
	public boolean isPhraseMandatory() {
		String clas = phraseLabel.getAttribute("class");
		Assert.assertEquals(clas.contains("required"), true); 
		return true;
	}

	/*Method verify mood dropdown contains certain values 
	 * 
	 * 
	 */
	public void verifyMoodValues(String[] values) {
		List<WebElement> d = WebActionUtil.getDropDownOptions(moodDropdown);
		for(int i=0;i<values.length;i++) {
			for(WebElement element : d) {
				Assert.assertEquals(element.getText(), values[i], "element not present in dropdown");
			}
		}

	}

	/*Method to verify fields in conversation tab 
	 * 
	 * 
	 */
	public boolean verifyFieldsInAddConversationTab() {
		WebActionUtil.isElementDisplayedOrNot(cancel);
		WebActionUtil.isElementDisplayedOrNot(submitButton);
		WebActionUtil.isElementDisplayedOrNot(email);
		WebActionUtil.isElementDisplayedOrNot(trigger);
		WebActionUtil.isElementDisplayedOrNot(within);
		WebActionUtil.isElementDisplayedOrNot(repeatedMoreThan);
		WebActionUtil.isElementDisplayedOrNot(alertCheckbox);
		WebActionUtil.isElementDisplayedOrNot(record);
		WebActionUtil.isElementDisplayedOrNot(uploadImage);		
		WebActionUtil.isElementDisplayedOrNot(answerMedia);		
		WebActionUtil.isElementDisplayedOrNot(responseType);		
		WebActionUtil.isElementDisplayedOrNot(subjectDropdown);		
		WebActionUtil.isElementDisplayedOrNot(moodDropdown);		
		WebActionUtil.isElementDisplayedOrNot(questionTextbox);
		return true;
	}

	/*Method to get patient name displayed in UI
	 * 
	 * 
	 */
	public String getPatientName() {
		String patient_name=patientName.getText();
		return patient_name;
	}

	/*Method to verify navigation to addNew conversation page 
	 * 
	 * 
	 */
	public boolean verifyAddNewConversationPageNavigation() {
		WebActionUtil.isElementDisplayedOrNot(questionTextbox);
		WebActionUtil.isElementDisplayedOrNot(moodDropdown);
		return true;
	}

	/*Method to check whether patient name is read-only
	 * 
	 * 
	 */
	public boolean isPatientNameReadOnly() {
		String attribute = patientName.getAttribute("ng-disabled");
		if(attribute.equalsIgnoreCase("disabled")) {
			return true;
		}
		return false;
	}

	/*Method to verify whether all fields in conversation page is displayed or not 
	 * 
	 * 
	 */
	public boolean isConversationPageFieldsDisplayed() {
		WebActionUtil.isElementDisplayedOrNot(patientName);
		WebActionUtil.isElementDisplayedOrNot(addNewButton);
		WebActionUtil.isElementDisplayedOrNot(table);
		return true;
	}

	/*Method to verify searched record displays    
	 * 
	 * 
	 */
	
	/*Method to check whether all fields in personalized media library page is present 
	 * 
	 * 
	 */
	public void verifyPersonalizedmediaLibraryFields() {
		WebActionUtil.isElementDisplayedOrNot(personalizedSearchTextbox);
		WebActionUtil.isElementDisplayedOrNot(personalizedSearchButton);
		WebActionUtil.isElementDisplayedOrNot(table);
		WebActionUtil.isElementDisplayedOrNot(allDropdown);
		WebActionUtil.isElementDisplayedOrNot(Refreshbutton);
	}
	
	/*Method to check record searched is displayed
	 * 
	 * 
	 */
	public void  verifySearchMedia(String record) throws Throwable {
		WebActionUtil.typeText(filterTextBox, record, "");
		WebActionUtil.clickOnElementUsingJS(personalizedSearchButton, "Search Button");
		List<WebElement> nameColumnElementsList = this.driver.findElements(By.cssSelector("div[id^='form-group-Name']"));
		for(WebElement element:nameColumnElementsList) {
			Assert.assertEquals(element.getText().contains(record), true);
			Assert.assertEquals(element.isDisplayed(), true);
		}
	}

	/*Method to check record in personalized media table could be deleted 
	 * 
	 * 
	 */
	public boolean verifyMediaRecordsDeletionAccess(String name) {
		List<WebElement> nameColumnElementsList = this.driver.findElements(By.cssSelector("div[id^='form-group-Name']"));

		for(int i=2;i<nameColumnElementsList.size();i++) {

			if(nameColumnElementsList.get(i).getText().equalsIgnoreCase(name)) {
				driver.findElement(By.xpath("//div[contains(@class,'formio-data-grid')]/table[contains(@class,'datagrid-table')]/tbody/tr[i]/td[last()]")).click();

				// alert pop-up
				WebActionUtil.isAlertPresent(driver, 10);

				return true;
			}
		}
		return false;
	}

	/*Method to verify error message in PersonalizedMedia Tab when no records present
	 * 
	 * 
	 */
	public void verifyMessageInPersonalizedMediaTab(String message) {
		WebActionUtil.verifyElementText(errorMessage, message);
	}

	/*Method to verify navigation to music and movies tab   
	 * 
	 * 
	 */
	public boolean verifyNavigationToMusicTab(String tabName) {
		WebActionUtil.isElementDisplayedOrNot(pesonalizedMediaTab);
		return true;
	}

	/*Method to verify navigation to PersonalizedMedia Tab
	 * 
	 * 
	 */
	public boolean verifynavigationToPersonalizedmediatab(){
		WebActionUtil.isElementDisplayedOrNot(pesonalizedMediaLibraryTab);
		return true;
	}

	/*Method to verify navigation to conversation Tab
	 * 
	 * 
	 */
	public boolean verifynavigationToConversationTab() {
		WebActionUtil.isElementDisplayedOrNot(conversationTab);
		return true;
	}

	/*Method to verify navigation to personal interest Tab
	 * 
	 * 
	 */
	public boolean verifynavigationToPersonalInterestTab() {
		WebActionUtil.isElementDisplayedOrNot(personalInterestTab);
		return true;
	}
	
	public void isSearchDisplayed() {
		
	}
}