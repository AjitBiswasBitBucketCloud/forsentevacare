package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;


public class Eva_CCA_Onboard_Patient_079 extends BaseTest{
	//Test data
	private InitializePages initializePages;
	private final String TAG = "#@$%1236";
	private final String PATIENT_NAME = "vvv";
	private final String COLUMN_NAME = "Personalized Media";

	/*
	 * Test to validate text fields in personalized media page 
	 * 
	 */
	@Test
	public void testDataValidation() throws Throwable {
		// Initialization of page objects
		initializePages = new InitializePages(driver);

		// Click on OnBoard patient 
		initializePages.homepage.clickOnBoardpatient();

		// Click add button under music and movies 
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();

		// Assert navigation to personal media page 
		Assert.assertEquals(initializePages.onboardPatientpage.verifyNavigationToMusicTab(COLUMN_NAME), true, "Navigation to Music and movies tab failed");

		// Remove data on each portlet
		initializePages.onboardPatientpage.removeTagsData();

		// Enter data into tags and add data 
		initializePages.onboardPatientpage.enterTags(TAG);
		
		// Assert whether enter data into tags is accepted
		String actualTag = initializePages.onboardPatientpage.getTagText();
		Assert.assertEquals(actualTag, TAG, "Tag did not accept special characters");
	}

	/*
	 * Method to clean up data in in music and movies tab 
	 * 
	 */
	public void cleanUp() throws Throwable {
		// Initialization of page objects
		initializePages = new InitializePages(driver);
		
		// Click on OnBoard patient 
		initializePages.homepage.clickOnBoardpatient();
		
		// Click add button under music and movies 
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();
		
		// Remove data from each portlet
		initializePages.onboardPatientpage.removeDataFromAllPortlets();
	}
}