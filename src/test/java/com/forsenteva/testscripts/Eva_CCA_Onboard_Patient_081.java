package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class Eva_CCA_Onboard_Patient_081 extends BaseTest{
	// Test data 
	private final String PATIENT_NAME = "P4_aaa";
	private final String TAG = "aaaaaaa";
	private final String MUSIC_MOVIES = "Music and Movies";

	/*
	 * Test to verify frequency of data in each portlet in personalized media page 
	 * 
	 */
	@Test
	public void verifyUserCountInPortlet() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);

		// click on on board patient icon in home page  Music and Movies
		initializePages.onboardPatientpage.clickOnBoardpatient();
		// click on add button under music and movies for patient P4_aaa
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();
		Assert.assertEquals(initializePages.onboardPatientpage.verifyNavigationToMusicTab(MUSIC_MOVIES), true, "Navigation to music and movies Failed");
		//initializePages.onboardPatientpage.validateMediaTags(MUSIC_MOVIES);
	}
}