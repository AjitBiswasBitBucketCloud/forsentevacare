package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.WebActionUtil;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class Eva_CCA_Onboard_Patient_082 extends BaseTest{
	// Test data 
	private final String PAGE_TITLE = "Onboard Patient - Eva";
	private final String PATIENT_NAME = "P4_aaa";
	
	/*
	 * Test to verify page navigation to personalized media library page 
	 * 
	 */
	@Test
  public void verifyPageNavigation() throws Throwable {
		// Initialization of all page objects 
		InitializePages initializePages = new InitializePages(driver);
		
		 // click on on board patient icon in home page  Music and Movies
		initializePages.homepage.clickOnBoardpatient();
		
		 // click on add button under music and movies for patient P4_aaa
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();
		 
		// Assert page navigation to personalized media library page
		Assert.assertEquals(initializePages.onboardPatientpage.verifynavigationToPersonalizedmediatab(), true, "Page did not get navigated to personalized media library page");
  }
}