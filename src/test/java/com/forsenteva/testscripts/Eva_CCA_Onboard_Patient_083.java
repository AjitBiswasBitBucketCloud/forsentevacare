package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;

public class Eva_CCA_Onboard_Patient_083 extends BaseTest{
	// Test data 
		private final String PAGE_TITLE = "Onboard Patient - Eva";
		private final String PATIENT_NAME = "P4_aaa";
		private final String COLUMN_NAME = "Music and Movies";
		
   /*
	* Test to verify personalized media library page conatins all relevant fields 
    * 
	*/	
	@Test
	public void verifyFieldsDisplay() throws Throwable {
		// Initialization of all page objects 
		InitializePages initializePages = new InitializePages(driver);
		
		// click on on board patient icon in home page  Music and Movies
		initializePages.homepage.clickOnBoardpatient();
		
		 // click on add button under music and movies for patient P4_aaa
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();
		
		 // verify page elements
		initializePages.onboardPatientpage.verifyPersonalizedmediaLibraryFields();
	}
}