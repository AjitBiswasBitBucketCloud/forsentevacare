package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_084 extends BaseTest{
	// Test data
	public final String PATIENT = "vvv";
	public final String PERSONAL_INTERESTS = "Personal Interests";
	public final String MUSIC_MOVIES = "Music and Movies";
	public final String PERSONALIZED_MEDIA = "Personalized Media Library";
	public final String NO_RECORDS_FOUND = "No Results Found";

	/*
	 * Test to verify error message in pesonalized media library page when no records present
	 */
	@Test
	public void verifyPrsonalizedMediaPageWithoutData() throws Throwable {
		// Initialization of page objects
		InitializePages initializePages = new InitializePages(driver);

		 // click on board patient in On Board page 
		initializePages.homepage.clickOnBoardpatient();

		 // click add button under personal interest column for patient vvv
		initializePages.onboardPatientpage.clickPersonalInterestAdd();

		// Assert navigation to personal interest page
		Assert.assertEquals(initializePages.onboardPatientpage.verifynavigationToPersonalInterestTab(), true, "");
		
		initializePages.onboardPatientpage.removeDataFromAllPortlets();

		/*
		 * click on add button under music and movies column in On Board page 
		 */
		initializePages.onboardPatientpage.clickMusicAndMoviesAdd();

		/*
		 * Assert navigation to personal interest page after clicking on it 
		 */
		Assert.assertEquals(initializePages.onboardPatientpage.verifyNavigationToMusicTab(PERSONAL_INTERESTS), true);

		/*
		 * Remove data under all text boxes
		 */
		initializePages.onboardPatientpage.removeDataFromAllPortlets();

		/*
		 * Click add button under personalized media column
		 */
		initializePages.onboardPatientpage.clickPersnalizedMediadAdd();
		initializePages.onboardPatientpage.clickRefresh();

		/*
		 * Verify no records found message
		 */
		initializePages.onboardPatientpage.verifyMessageInPersonalizedMediaTab(NO_RECORDS_FOUND);
	}
}
