package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_088 extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String PATIENT = "vvv";
	public final String PERSONALIZED_MEDIA = "Personalized Media Library";
	public final String NO_RECORDS_FOUND = "No Results Found";
	public final String MEDIA_RECORD_NAME = "AROUND";
	
	/*
	 * Initialization of all page page objects 
	 */
	
  @Test
  public void verifyMediaRecordsDeletionAccess() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	    /*
		 * click on board patient in On Board page 
		 */
		initializePages.homepage.clickOnBoardpatient();
		
		/*
		 * click add button under personal interest column for patient vvv
		 */
		//initializePages.onboardPatientpage.clickCellDataInOnBoardPage();

		/*
		 * Assert navigation to personal interest page after clicking on it 
		 */
		// Assert.assertEquals(initializePages.onboardPatientpage.verifyNavigationToTab(PERSONALIZED_MEDIA), true);
		
		// initializePages.onboardPatientpage.verifyMediaRecordsDeletionAccess(MEDIA_RECORD_NAME);

		Assert.assertEquals(initializePages.onboardPatientpage.verifyMediaRecordsDeletionAccess(MEDIA_RECORD_NAME), true);
  }
}
