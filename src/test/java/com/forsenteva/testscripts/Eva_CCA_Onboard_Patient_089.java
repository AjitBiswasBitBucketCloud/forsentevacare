package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_089 extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String PATIENT = "vvv";
	public final String PERSONALIZED_MEDIA = "Personalized Media Library";
	
	/*
	 * Initialization of all page page objects 
	 */
	
	@Test
  public void verifyMediaRecordSearch() throws Throwable {
		/*
		 * click on board patient in On Board page 
		 */
		InitializePages initializePages = new InitializePages(driver);
		initializePages.homepage.clickOnBoardpatient();
		
		/*
		 * click add button under personal interest column for patient 
		 */
		//initializePages.onboardPatientpage.clickCellDataInOnBoardPage();

		/*
		 * Assert navigation to personal interest page after clicking on it 
		 */
		//Assert.assertEquals(initializePages.onboardPatientpage.verifyNavigationToTab(PERSONALIZED_MEDIA), true);
  }
}
