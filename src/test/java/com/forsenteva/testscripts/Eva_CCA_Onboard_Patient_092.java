package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_092 extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String PATIENT = "vvv";
	public final String CONVERSTAION = "Conversation";

	/*
	 * Initialization of all page page objects 
	 */
	

	@Test
	public void f() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		/*
		 * click on board patient in On Board page 
		 */
		initializePages.homepage.clickOnBoardpatient();

		/*
		 * click add button under conversation column for patient 
		 */
		//initializePages.onboardPatientpage.clickCellDataInOnBoardPage();

		/*
		 * verify Navigation To Tab 
		 */
		//initializePages.onboardPatientpage.verifyNavigationToTab(CONVERSTAION);

		/*
		 * verify whether patient name field is read-only  
		 */
		Assert.assertEquals(initializePages.onboardPatientpage.isPatientNameReadOnly(), true);

		/*
		 * verify whether patient name displayed is one which is selected  
		 */
		Assert.assertEquals(initializePages.onboardPatientpage.getPatientName(), PATIENT);
	}
}