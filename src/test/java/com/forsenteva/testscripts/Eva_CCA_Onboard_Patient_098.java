package com.forsenteva.testscripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_098 extends BaseTest{
	/*
	 * Defining all test data
	 */
	private final String PHRASE = "";
	private final String PATIENT = "vvv";
	private final String MOOD = "Agitated";
	private final String CONVERSTAION = "Conversation";
	private final String SUBJECT = "Baseball Sport";

	/*
	 * Initialization of all page page objects 
	 */
	//InitializePages initializePages = new InitializePages(driver);

	@Test
	public void validatePhrase() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		/*
		 * click on board patient in On Board page 
		 */
		initializePages.homepage.clickOnBoardpatient();

		/*
		 * click add button under conversation column for patient 
		 */
		//initializePages.onboardPatientpage.clickCellDataInOnBoardPage();

		/*
		 * verify Navigation To Tab 
		 */
		//initializePages.onboardPatientpage.verifyNavigationToTab(CONVERSTAION);

		/*
		 *  click on Add new 
		 */
		initializePages.onboardPatientpage.clickAddNew();

		/*
		 *  Assert navigation to Add New Conversation page 
		 */		
		Assert.assertEquals(initializePages.onboardPatientpage.verifyAddNewConversationPageNavigation(), true, "Page has not navigated to conversation page");
		
		/*
		 *  Validate phrase field
		 */
		initializePages.onboardPatientpage.validatePhrase(PHRASE, MOOD, SUBJECT);	
	}
}
