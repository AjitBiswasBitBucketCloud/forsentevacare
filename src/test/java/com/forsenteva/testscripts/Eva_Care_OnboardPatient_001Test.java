package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
public class Eva_Care_OnboardPatient_001Test extends BaseTest {
	@Test
	public void verifyOnboardPatientTabTest() throws Throwable {
		
		InitializePages.OnboardPatientPage.verifyOnboardPatientTab();
		
	}

}
