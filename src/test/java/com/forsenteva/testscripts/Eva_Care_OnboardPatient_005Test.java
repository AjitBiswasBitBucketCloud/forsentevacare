package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.pages.HomePage;

public class Eva_Care_OnboardPatient_005Test extends BaseTest{
@Test
public void checkingFirstnameFieldTest() throws Throwable {
	
	
	
	String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
	
	 
		 String sdata2 = ExcelDataProvider.readcolData(sdata1[9], "FName", GenericLib.testDataPath, "Sheet1");	 
			
						
		String sdata3 = ExcelDataProvider.readcolData(sdata1[9], "Lname", GenericLib.testDataPath, "Sheet1");	 
				
		String sdata4 = ExcelDataProvider.readcolData(sdata1[9], "Email", GenericLib.testDataPath, "Sheet1");	 
					
		 String sdata5 = ExcelDataProvider.readcolData(sdata1[9], "phone", GenericLib.testDataPath, "Sheet1");	 
						
		String sdata6 = ExcelDataProvider.readcolData(sdata1[9], "Ephone", GenericLib.testDataPath, "Sheet1");	 
							
		String sdata7 = ExcelDataProvider.readcolData(sdata1[9], "Econtact", GenericLib.testDataPath, "Sheet1");	 
						
		InitializePages.OnboardPatientPage.checkingFirstnameField(sdata2, sdata3, sdata4, sdata5, sdata6, sdata7);
	 
	
}
}
