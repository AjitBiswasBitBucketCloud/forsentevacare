package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;


public class Eva_Care_OnboardPatient_014Test extends BaseTest{
	@Test
	public void emailValidationMessageTest() throws Throwable
	{
		
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
		
			  
					
			String sdata4 = ExcelDataProvider.readcolData(sdata1[1], "Email", GenericLib.testDataPath, "Sheet1");	 
						
				 
							
			InitializePages.OnboardPatientPage.emailValidationMessage(sdata4);
		 
		
	}

}
