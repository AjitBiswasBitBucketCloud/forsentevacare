package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_Care_OnboardPatient_018Test extends BaseTest {
	@Test
	public void checkMultipleDataInGenderTest() throws Throwable {
		InitializePages.OnboardPatientPage.checkMultipleDataInGender();	
	}

}
