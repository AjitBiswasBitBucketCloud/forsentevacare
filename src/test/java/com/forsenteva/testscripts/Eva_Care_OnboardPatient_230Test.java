package com.forsenteva.testscripts;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_Care_OnboardPatient_230Test extends BaseTest{
	
@Test
public void verifyEventModificationTest() throws Throwable
{
	InitializePages.OnboardPatientPage.verifyEventModification();
}
}
